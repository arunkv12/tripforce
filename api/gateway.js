import axios from "axios";

export async function async_get(params) {
  let result = {};

  await axios({
    method: "get",
    headers: getHeader(),
    url: process.env.api + params.apiurl,
    params: params,
    timeout: 60000,
  })
    .then((response) => {
      result.status = response.status;
      //console.log(response);
      //setContactLoading(false);
      if (response.status === 200) {
        result = response;
      }
    })
    .catch((error) => {
      result.status = error.response.status;
      result.data = error.response.data;
    });

  return result;
}

export async function async_get_with_pathparam(pathparam, params) {
  let result = {};

  await axios({
    method: "get",
    headers: getHeader(),
    url: prepareUrl(process.env.api + params.apiurl, pathparam),
    params: params,
    timeout: 60000,
  })
    .then((response) => {
      result.status = response.status;
      if (response.status === 200) {
        result.data = response.data;
      }
    })
    .catch((error) => {
      result.status = error.response.status;
      result.data = error.response.data;
    });
  return result;
}

export async function async_post(data, params) {
  let result = {};
  await axios({
    method: "post",
    headers: getHeader(),
    url: process.env.api + params.apiurl,
    params: params,
    data: data,
    timeout: 60000,
  })
    .then((response) => {
      result.status = response.status;
      if (response.status === 200 || response.status === 201) {
        result.data = response.data;
      }
    })
    .catch((error) => {
      result.status = error.response.status;
      result.data = error.response.data;
    });
  return result;
}

export async function async_post_with_pathparam(pathparam, data, params) {
  let result = {};

  await axios({
    method: "post",
    headers: getHeader(),
    url: prepareUrl(process.env.api + params.apiurl, pathparam),
    params: params,
    data: data,
    //timeout: 60000
  })
    .then(function (response) {
      result.status = response.status;
      if (response.status === 200 || response.status === 201) {
        result.data = response.data;
      }
    })
    .catch(function (error) {
      result.status = error.response.status;
      result.data = error.response.data;
    });
  return result;
}

export async function async_put(data, params) {
  let result = {};
  await axios({
    method: "put",
    headers: getHeader(),
    url: process.env.api + params.apiurl,
    params: params,
    data: data,
    timeout: 60000,
  })
    .then((response) => {
      result.status = response.status;
      if (response.status === 200) {
        result.data = response.data;
      }
    })
    .catch((error) => {
      result.status = error.response.status;
      result.data = error.response.data;
    });
  return result;
}

export async function async_put_with_pathparam(pathparam, data, params) {
  let result = {};

  await axios({
    method: "put",
    headers: getHeader(),
    url: prepareUrl(process.env.api + params.apiurl, pathparam),
    params: params,
    data: data,
    timeout: 60000,
  })
    .then((response) => {
      result.status = response.status;
      if (response.status === 200) {
        result.data = response.data;
      }
    })
    .catch((error) => {
      result.status = error.response.status;
      result.data = error.response.data;
    });
  return result;
}

export async function async_delete_with_pathparam(pathparam, params) {
  let result = {};

  await axios({
    method: "delete",
    headers: getHeader(),
    url: prepareUrl(process.env.api + params.apiurl, pathparam),
    params: params,
    timeout: 60000,
  })
    .then(function (response) {
      result.status = response.status;
      if (response.status === 200) {
        result.data = response.data;
      }
    })
    .catch(function (error) {
      result.status = error.response.status;
      result.data = error.response.data;
    });
  return result;
}

const getHeader = () => {
  let header = { "Content-Type": "application/json" };

  if (
    localStorage.getItem("app_token") != undefined &&
    localStorage.getItem("app_token").length > 0
  ) {
    header.authorization = localStorage.getItem("app_token");
  }

  return header;
};

const prepareUrl = (url, pathparam) => {
  Object.entries(pathparam).forEach(([key, value]) => {
    url = url.replace(key, value);
  });

  return url;
};
