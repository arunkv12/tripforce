module.exports = {
  masters: "/masters",
  masters_trip_points: "/masters/trip_points",
  bookings: "/bookings",

  users_auth: "/users/auth",
  admin_trips: "/admin/trips",
  admin_trips_quotations: "/admin/trips/quotations/path1",

  customer_trips: "/customer/trips/path1",
  customer_trips_quotations: "/customer/trips/quotations/path1",
};
