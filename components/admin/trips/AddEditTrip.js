import {
  Col,
  Row,
  Form,
  Select,
  AutoComplete,
  DatePicker,
  InputNumber,
  Input,
  Button,
  Checkbox,
  message,
} from "antd";
import { useEffect, useState } from "react";
import { async_get as get, async_post as post } from "../../../api/gateway";
import url from "../../../api/url";
import moment from "moment";
import { getDateString } from "../../../util/date";

export default function AddEditTrip(props) {
  const { Option } = AutoComplete;
  const { RangePicker } = DatePicker;
  const [form] = Form.useForm();

  const [tripStartPoints, setTripStartPoints] = useState([]);
  const [tripEndPoints, setTripEndPoints] = useState([]);
  const [tripTransferPoints, setTripTransferPoints] = useState([]);
  const [tripTransferPointsDS, setTripTransferPointsDS] = useState([]);
  const [vehicleTypes, setVehicleType] = useState([]);

  useEffect(() => {
    get({ apiurl: url.masters, group_code: "vt" }).then((response) => {
      if (response.status === 200) {
        setVehicleType(response.data);
      }
    });

    if (props.mode === "edit") {
      setTripStartPoints([
        {
          place_id: props.data.trip_start_pt,
          description: props.data.trip_start_pt_desc,
        },
      ]);
      setTripEndPoints([
        {
          place_id: props.data.trip_end_pt,
          description: props.data.trip_end_pt_desc,
        },
      ]);

      let tripTransferPointsDesc = "";
      if (
        props.data.trip_transfer_points != undefined &&
        props.data.trip_transfer_points.length > 0
      ) {
        let tripTransferPoints = props.data.trip_transfer_points.split(",");
        tripTransferPointsDesc = props.data.trip_transfer_points_desc.split(
          "###,"
        );
        let tripTransferPointsArr = [];
        tripTransferPoints.forEach((item, index) => {
          if (
            tripTransferPointsDesc[index] != undefined &&
            tripTransferPointsDesc[index].length > 0
          ) {
            let point = {};
            point.point_id = item;
            point.description = tripTransferPointsDesc[index];
            tripTransferPointsArr.push(point);
          }
        });
        setTripTransferPoints(tripTransferPointsArr);
      }

      form.setFieldsValue({
        trip_start_pt: props.data.trip_start_pt_desc,
        trip_end_pt: props.data.trip_end_pt_desc,

        head_count: props.data.head_count,
        vehicle_type: props.data.vehicle_type,
        remarks: props.data.remarks,
        contact_email: props.data.contact_email,
        contact_first_name: props.data.contact_first_name,
        contact_last_name: props.data.contact_last_name,
        contact_mobile_num: props.data.contact_mobile_num,
        trip_date: [
          moment(props.data.trip_start_date),
          moment(props.data.trip_end_date),
        ],
      });

      if (tripTransferPointsDesc.length > 0) {
        form.setFieldsValue({
          trip_transfer_points: tripTransferPointsDesc,
        });
      }
    }

    form.setFieldsValue({ customer_notification: true });
  }, []);

  const onSearchTripPoints = (value, type) => {
    if (value != undefined && value.length > 0) {
      get({ apiurl: url.masters_trip_points, input: value }).then(
        (response) => {
          if (response.status === 200) {
            if (type === "start") {
              setTripStartPoints(response.data);
            } else if (type === "end") {
              setTripEndPoints(response.data);
            } else {
              /* setTripTransferPointsDS(
                tripTransferPointsDS.concat(response.data)
              ); */
              setTripTransferPoints(response.data);
            }
          }
        }
      );
    }
  };

  const saveBooking = (values) => {
    values.trip_start_pt_desc = values.trip_start_pt;
    values.trip_start_pt = tripStartPoints.filter(
      (item) => item.description === values.trip_start_pt
    )[0].place_id;

    values.trip_end_pt_desc = values.trip_end_pt;
    values.trip_end_pt = tripEndPoints.filter(
      (item) => item.description === values.trip_end_pt
    )[0].place_id;

    if (
      values.trip_transfer_points != undefined &&
      values.trip_transfer_points.length > 0
    ) {
      let transferPoints = tripTransferPoints.filter((point) => {
        return values.trip_transfer_points.find((place_id) => {
          return point.place_id === place_id;
        });
      });

      //console.log(transferPoints);
      //let jsonObj = transferPoints.map(JSON.stringify);

      //let uSet = new Set(jsonObj);
      //let uArr = Array.from(uSet).map(JSON.parse);
      //console.log(uArr);
      values.trip_transfer_points_desc = transferPoints
        .map((point) => {
          return point.description;
        })
        .toString();
      values.trip_transfer_points = values.trip_transfer_points.toString();
    } else {
      values.trip_transfer_points = "";
    }

    if (values.trip_date != undefined && values.trip_date.length > 0) {
      values.trip_start_date = getDateString(values.trip_date[0]);
      values.trip_end_date = getDateString(values.trip_date[1]);
    }

    values.contact_mobile_country_cd =
      values.contact_mobile_num != undefined &&
      values.contact_mobile_num.length > 0
        ? "+91"
        : "";

    values.trip_type = "rfq";
    values.booking_source = "user";

    let params = {};
    params.apiurl = url.admin_trips;

    console.log(values);

    if (props.mode === "add") {
      post(values, params).then((response) => {
        if (response.status === 201) {
          message.success("New Trip created successfully!");
          props.closeDrawer();
        } else {
          message.error("Failed to create Trip, please try again!");
        }
      });
    } else {
    }
  };

  return (
    <Form size="large" form={form} onFinish={saveBooking}>
      {props.mode === "edit" && (
        <h2
          style={{
            fontWeight: "300",
          }}
        >
          {props.data.trip_num}
        </h2>
      )}

      <h2>Travel Plan</h2>
      <Col span={24}>
        <Row gutter={16}>
          <Col xs={24} md={5}>
            <Form.Item
              name="trip_start_pt"
              rules={[
                {
                  required: true,
                  message: "'Trip Start Point' is invalid",
                },
              ]}
            >
              <AutoComplete
                allowClear
                filterOption={(input, option) =>
                  option.props.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) >= 0
                }
                placeholder="Type Trip Start Point"
                onSearch={(value) => onSearchTripPoints(value, "start")}
              >
                {tripStartPoints.map((item) => (
                  <Option key={item["place_id"]} value={item["description"]}>
                    {item["description"]}
                  </Option>
                ))}
              </AutoComplete>
            </Form.Item>
          </Col>
          <Col xs={24} md={5}>
            <Form.Item
              name="trip_end_pt"
              rules={[
                {
                  required: true,
                  message: "'Trip End Point' is required",
                },
              ]}
            >
              <AutoComplete
                allowClear
                filterOption={(input, option) =>
                  option.props.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) >= 0
                }
                placeholder="Type Trip End Point"
                onSearch={(value) => onSearchTripPoints(value, "end")}
              >
                {tripEndPoints.map((item) => (
                  <Option key={item["place_id"]} value={item["description"]}>
                    {item["description"]}
                  </Option>
                ))}
              </AutoComplete>
            </Form.Item>
          </Col>
          <Col xs={24} md={6}>
            <Form.Item
              name="trip_date"
              rules={[
                {
                  required: true,
                  message: "'Trip Dates' is invalid",
                },
              ]}
            >
              <RangePicker
                style={{ width: "100%" }}
                disabledDate={(currentDate) =>
                  currentDate < moment().subtract(1, "days")
                }
                format="YYYY-MM-DD"
              />
            </Form.Item>
          </Col>

          <Col xs={24} md={6}>
            <Form.Item name="trip_transfer_points">
              <Select
                placeholder="Type Trip Transfer Points"
                mode="multiple"
                allowClear
                showSearch
                filterOption={(input, option) =>
                  option.props.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) >= 0
                }
                onSearch={(value) => onSearchTripPoints(value, "transfer")}
              >
                {tripTransferPoints.map((item) => (
                  <Option value={item["place_id"]}>
                    {item["description"]}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>
      </Col>

      <h2>Travel Preference</h2>

      <Col span={24}>
        <Row gutter={16}>
          <Col xs={24} md={5}>
            <Form.Item name="head_count">
              <InputNumber
                min={2}
                max={100}
                style={{ width: "100%" }}
                placeholder="Number Of Passengers"
              ></InputNumber>
            </Form.Item>
          </Col>

          <Col xs={24} md={5}>
            <Form.Item name="vehicle_type">
              <Select
                placeholder="Choose Vehicle Type"
                allowClear
                showSearch
                filterOption={(input, option) =>
                  option.props.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) >= 0
                }
              >
                {vehicleTypes.map((item) => (
                  <Option value={item["reference_code"]}>
                    {item["reference_desc_en"]}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>

          <Col xs={24} md={5}>
            <Form.Item name="remarks">
              <Input placeholder="Any Special Remarks"></Input>
            </Form.Item>
          </Col>
        </Row>
      </Col>

      <h2>Contact Details</h2>

      <Col span={24}>
        <Row gutter={16}>
          <Col xs={24} md={5}>
            <Form.Item
              name="contact_first_name"
              rules={[
                {
                  required: true,
                  max: 10,
                  message: "'Firstname' is invalid",
                },
              ]}
            >
              <Input placeholder="Firstname" />
            </Form.Item>
          </Col>

          <Col xs={24} md={5}>
            <Form.Item
              name="contact_last_name"
              rules={[
                {
                  required: true,
                  max: 10,
                  message: "'Lastname' is invalid",
                },
              ]}
            >
              <Input placeholder="Lastname" />
            </Form.Item>
          </Col>
          <Col xs={24} md={5}>
            <Form.Item
              name="contact_mobile_num"
              rules={[
                {
                  required: true,
                  len: 10,
                  pattern: "^[0-9]+$",
                  message: "'Mobile' is invalid",
                },
              ]}
            >
              <Input addonBefore="+91" placeholder="Mobile" />
            </Form.Item>
          </Col>

          <Col xs={24} md={5}>
            <Form.Item
              name="contact_email"
              rules={[
                {
                  required: true,
                  type: "email",
                  message: "'Email' is invalid",
                },
              ]}
            >
              <Input placeholder="Email" />
            </Form.Item>
          </Col>
        </Row>
      </Col>

      <Col xs={24} md={8}>
        <Form.Item name="customer_notification" valuePropName="checked">
          <Checkbox>Send mail notification to Contact Email</Checkbox>
        </Form.Item>
      </Col>

      <Row justify="center">
        <Col xs={24} md={8}>
          <Button
            block
            type="primary"
            htmlType="submit"
            className="mt-20"
            //onClick={saveBooking}
          >
            Save Trip
          </Button>
        </Col>
      </Row>
    </Form>
  );
}
