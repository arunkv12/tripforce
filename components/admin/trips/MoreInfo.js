import { Descriptions, Row, Button } from "antd";
import { formatToDate, formatToDateTime } from "../../../util/date";
import { EditOutlined } from "@ant-design/icons";
import { useState } from "react";
import AddEditTrip from "./AddEditTrip";

export default function MoreInfo(props) {
  const [editMode, setEditMode] = useState(false);

  const onClickEdit = () => {
    setEditMode(true);
  };
  return (
    <>
      {!editMode && (
        <Row align="middle">
          <h2
            style={{
              fontWeight: "300",
            }}
          >
            {props.data.trip_num}
          </h2>

          <Button
            style={{ marginLeft: "5px", marginBottom: "25px" }}
            size="small"
            icon={<EditOutlined />}
            onClick={() => onClickEdit()}
          >
            Edit
          </Button>
        </Row>
      )}

      {editMode ? (
        <AddEditTrip mode="edit" data={props.data} />
      ) : (
        <>
          <Descriptions
            className="highlight-description"
            column={{ xs: 1, md: 3 }}
          >
            <Descriptions.Item label="Trip Type">
              {props.data.trip_type_desc}
            </Descriptions.Item>
            <Descriptions.Item label="Requestd On">
              {formatToDateTime(props.data.created_on, "default")}
            </Descriptions.Item>
            <Descriptions.Item label="Trip Start Date">
              {formatToDate(props.data.trip_start_date)}
            </Descriptions.Item>
            <Descriptions.Item label="Trip End Date">
              {formatToDate(props.data.trip_end_date)}
            </Descriptions.Item>
            <Descriptions.Item label="Trip Start Point">
              {props.data.trip_start_pt_desc}
            </Descriptions.Item>
            <Descriptions.Item label="Trip End Point">
              {props.data.trip_end_pt_desc}
            </Descriptions.Item>
            <Descriptions.Item label="Trip Transfer Points">
              {props.data.trip_transfer_points_desc != undefined &&
                props.data.trip_transfer_points_desc.length > 0 &&
                props.data.trip_transfer_points_desc.replaceAll("###", " ")}
            </Descriptions.Item>
            <Descriptions.Item label="Vehicle Type">
              {props.data.vehicle_type_desc}
            </Descriptions.Item>
            <Descriptions.Item label="Vehicle Count">
              {props.data.vehicle_count}
            </Descriptions.Item>
            <Descriptions.Item label="Remark">
              {props.data.remarks}
            </Descriptions.Item>
          </Descriptions>
          <Descriptions
            title="Contact Details"
            className="highlight-description"
            column={{ xs: 1, md: 4 }}
          >
            <Descriptions.Item label="Email">
              {props.data.contact_email}
            </Descriptions.Item>
            <Descriptions.Item label="First Name">
              {props.data.contact_first_name}
            </Descriptions.Item>
            <Descriptions.Item label="Last Name">
              {props.data.contact_last_name}
            </Descriptions.Item>
            <Descriptions.Item label="Mobile No">
              {props.data.contact_mobile_country_cd +
                props.data.contact_mobile_num}
            </Descriptions.Item>
          </Descriptions>
          <Descriptions
            title="Customer Details"
            className="highlight-description"
            column={{ xs: 1, md: 4 }}
          >
            <Descriptions.Item label="Email">
              {props.data.customer_email}
            </Descriptions.Item>
            <Descriptions.Item label="First Name">
              {props.data.customer_first_name}
            </Descriptions.Item>
            <Descriptions.Item label="Last Name">
              {props.data.customer_last_name}
            </Descriptions.Item>
            <Descriptions.Item label="Mobile No">
              {props.data.customer_mobile_num != undefined &&
              props.data.customer_mobile_num.length > 0
                ? props.data.customer_mobile_country_cd +
                  props.data.customer_mobile_num
                : ""}
            </Descriptions.Item>
          </Descriptions>
        </>
      )}
    </>
  );
}
