import {
  Col,
  List,
  Card,
  Row,
  Table,
  Form,
  Button,
  Space,
  Input,
  InputNumber,
  message,
} from "antd";
import { useState, useEffect } from "react";
import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
import { messages } from "../../../util/validations";
import {
  async_post_with_pathparam as postwithpath,
  async_get_with_pathparam as getwithpath,
} from "../../../api/gateway";
import url from "../../../api/url";
import { formatToDateTime } from "../../../util/date";
import { ViewIcon, DeleteIcon } from "../../common/Button_Icons";

export default function Quotations(props) {
  const [quotation_form] = Form.useForm();
  const [dataSource, setDataSource] = useState([]);
  const [addMode, setAddMode] = useState(false);
  const [viewDetailsMode, setViewDetailsMode] = useState(false);
  const [reload, setReload] = useState(false);
  const [dataSourceLoading, setDataSourceLoading] = useState(true);
  //const [totalAmount, setTotalAmount] = useState(1);
  const [quotationItems, setQuotationItems] = useState([]);
  const [saveDisabled, setSaveDisabled] = useState(false);

  useEffect(() => {
    setDataSourceLoading(true);
    quotation_form.setFieldsValue({
      items: [{ item: "", quantity: 1, rate: 1, amount: 1 }],
      to_email: props.data.to_email,
    });

    getwithpath(
      { path1: props.data.trip_num },
      {
        apiurl:
          props.mode === "view"
            ? url.customer_trips_quotations
            : url.admin_trips_quotations,
      }
    ).then((response) => {
      if (response.status === 200) {
        setDataSource(response.data);
      }
      setDataSourceLoading(false);
    });
  }, [reload]);

  const adminCols = [
    {
      title: "No",
      dataIndex: "quotation_num",
      ellipsis: true,
      render: (text, record) => (
        <a
          onClick={() => {
            setViewDetailsMode(true);
            setQuotationItems(record.tf_quotations_items);
          }}
        >
          {text}
        </a>
      ),
    },
    {
      title: "Created On",
      dataIndex: "created_on",
      ellipsis: true,
      render: (created_on) => {
        return formatToDateTime(created_on);
      },
    },

    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Space>
          <ViewIcon
            onClick={() => {
              setViewDetailsMode(true);
              setQuotationItems(record.tf_quotations_items);
            }}
          />
          <DeleteIcon />
        </Space>
      ),
    },
  ];

  const quotationItemsCols = [
    {
      title: "Item",
      dataIndex: "item",
      ellipsis: true,
    },
    {
      title: "Quantity",
      dataIndex: "quantity",
      ellipsis: true,
    },
    {
      title: "Rate",
      dataIndex: "rate",
      ellipsis: true,
    },
    {
      title: "Amount",
      dataIndex: "amount",
      ellipsis: true,
      render: (text, record) => record.quantity * record.rate,
    },
  ];

  const onFinish = (values) => {
    let quotationItems = [...quotation_form.getFieldsValue().items];
    quotationItems.map((item) => {
      item.status = 1;
      delete item.amount;
    });
    values.items = quotationItems;
    values.contact_email = props.data.contact_email;

    //console.log("Received values :", values);
    postwithpath({ path1: props.data.trip_num }, values, {
      apiurl: url.admin_trips_quotations,
    }).then((response) => {
      if (response.status === 201) {
        message.success("New Quotation created!");
        setAddMode(false);
        quotation_form.resetFields();
        setReload(!reload);
      } else {
        message.error("Failed to create Quotation, try again!");
      }
    });
  };

  const calculateAmount = (key) => {
    let quotationItems = [...quotation_form.getFieldsValue().items];
    let item = quotationItems[key];
    item.amount = item.quantity * item.rate;
    quotation_form.setFieldsValue({
      items: quotationItems,
    });
    calculateTotalAmount();
  };

  const calculateTotalAmount = () => {
    let quotationItems = [...quotation_form.getFieldsValue().items];
    let totalAmt = 0;
    //console.log(quotationItems);
    quotationItems.map((item) => {
      totalAmt = totalAmt + item.amount;
    });
    quotation_form.setFieldsValue({
      total_amount: totalAmt,
    });
    //console.log(totalAmt);
    totalAmt === 0 ? setSaveDisabled(true) : setSaveDisabled(false);
  };

  const calculateTotalAmountDisplay = () => {
    //let quotationItems = quotationItems;
    let totalAmt = 0;

    quotationItems.map((item) => {
      totalAmt = totalAmt + Number(item.rate) * item.quantity;
    });

    return totalAmt;
  };

  const Component = () => {
    if (addMode) {
      return (
        <>
          <div className="ant-descriptions-header ant-descriptions-title">
            New Quotation
          </div>
          <Form
            name="quotation_form"
            form={quotation_form}
            onFinish={onFinish}
            autoComplete="off"
            validateMessages={messages}
          >
            {/* <Row gutter={16}>
                  <Col xs={24} md={12}>
                    <InputDefault
                      required={true}
                      label="To Email"
                      name="to_email"
                    ></InputDefault>
                  </Col>
                  <Col xs={24} md={12}>
                    <Mobile label="Mobile No" name="to_mobile_no"></Mobile>
                  </Col>
                </Row>

                <Row gutter={16}>
                  <Col xs={24} md={12}>
                    <InputDefault
                      required={true}
                      label="First Name"
                      name="to_first_name"
                    ></InputDefault>
                  </Col>

                  <Col xs={24} md={12}>
                    <InputDefault
                      required={true}
                      label="Last Name"
                      name="to_last_name"
                    ></InputDefault>
                  </Col>
                </Row> */}

            <Row
              gutter={16}
              style={{
                margin: "0px 0px 20px",
                padding: "12px 0px 12px",
                backgroundColor: "#fafafa",
              }}
            >
              <Col xs={24} md={6}>
                Item
              </Col>
              <Col xs={0} md={6}>
                Quantity
              </Col>
              <Col xs={0} md={6}>
                Rate
              </Col>
              <Col xs={0} md={4}>
                Amount
              </Col>
              <Col xs={0} md={2}></Col>
            </Row>

            <Form.List name="items">
              {(fields, { add, remove }) => (
                <>
                  {fields.map(({ key, name, fieldKey, ...restField }) => (
                    <Row gutter={16}>
                      <Col xs={24} md={6}>
                        <Form.Item
                          {...restField}
                          name={[name, "item"]}
                          fieldKey={[fieldKey, "item"]}
                          rules={[
                            { required: true, message: "Item required!" },
                          ]}
                        >
                          <Input placeholder="Item Name (Required)" />
                        </Form.Item>
                      </Col>

                      <Col xs={24} md={6}>
                        <Form.Item
                          {...restField}
                          name={[name, "quantity"]}
                          fieldKey={[fieldKey, "quantity"]}
                          initialValue={1}
                          rules={[
                            {
                              required: true,
                              message: "Quantity required!",
                            },
                          ]}
                        >
                          <InputNumber
                            onChange={(e) => calculateAmount(key)}
                            min={1}
                            //max={99}
                            placeholder="Quanatity"
                          />
                        </Form.Item>
                      </Col>

                      <Col xs={24} md={6}>
                        <Form.Item
                          {...restField}
                          name={[name, "rate"]}
                          fieldKey={[fieldKey, "rate"]}
                          initialValue={1}
                          rules={[
                            { required: true, message: "Rate required!" },
                          ]}
                        >
                          <InputNumber
                            onChange={(e) => calculateAmount(key)}
                            min={1}
                            placeholder="Rate"
                          />
                        </Form.Item>
                      </Col>

                      <Col xs={24} md={4}>
                        <Form.Item
                          {...restField}
                          name={[name, "amount"]}
                          fieldKey={[fieldKey, "amount"]}
                          initialValue={1}
                        >
                          <Input prefix="₹" bordered={false} />
                        </Form.Item>
                      </Col>

                      <Col xs={24} md={2}>
                        <MinusCircleOutlined
                          onClick={() => {
                            remove(name);
                            calculateTotalAmount();
                          }}
                        />
                      </Col>
                    </Row>
                  ))}
                  <Form.Item>
                    <Button
                      type="dashed"
                      onClick={() => {
                        add();
                        //setTotalAmount(totalAmount + 1);
                        setSaveDisabled(false);
                      }}
                      block
                      icon={<PlusOutlined />}
                    >
                      Add New Item
                    </Button>
                  </Form.Item>
                </>
              )}
            </Form.List>

            <Row justify="end">
              <Col xs={24} md={4}>
                <Form.Item name="total_amount" initialValue={1}>
                  <Input
                    className="total-amt-custom"
                    prefix="₹"
                    bordered={false}
                  />
                </Form.Item>
              </Col>
            </Row>

            {/* <Row justify="end">
              <h2>Total &nbsp; &nbsp; ₹{totalAmount}</h2>
            </Row> */}

            <Row gutter={16}>
              <Col xs={24} md={12}>
                <Form.Item>
                  <Button block onClick={() => setAddMode(false)}>
                    Cancel
                  </Button>
                </Form.Item>
              </Col>
              <Col xs={24} md={12}>
                <Form.Item>
                  <Button
                    block
                    type="primary"
                    htmlType="submit"
                    disabled={saveDisabled}
                  >
                    Save
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </>
      );
    } else if (viewDetailsMode) {
      return (
        <>
          <div className="ant-descriptions-header ant-descriptions-title">
            {quotationItems[0].quotation_num}
          </div>
          <Table
            pagination={false}
            columns={quotationItemsCols}
            dataSource={quotationItems}
            size="middle"
          ></Table>

          <Row justify="end" style={{ marginTop: "24px" }}>
            <h2>Total &nbsp; &nbsp; ₹{calculateTotalAmountDisplay()}</h2>
          </Row>

          <Form>
            <Row gutter={16}>
              <Col xs={24} md={24}>
                <Form.Item>
                  <Button block onClick={() => setViewDetailsMode(false)}>
                    Back
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </>
      );
    } else {
      return (
        <Table
          columns={adminCols}
          dataSource={dataSource}
          size="middle"
          loading={dataSourceLoading}
        ></Table>
      );
    }
  };

  return (
    <>
      <Row justify="center">
        <Col xs={24} md={14}>
          {!viewDetailsMode && !addMode && props.mode != "view" && (
            <Row justify="end">
              <Button
                type="primary"
                style={{ marginBottom: "20px" }}
                onClick={() => setAddMode(true)}
              >
                Add New Quotation
              </Button>
            </Row>
          )}

          <Component />
        </Col>
      </Row>
    </>
  );
}
