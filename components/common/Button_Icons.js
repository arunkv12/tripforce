import { DeleteFilled, EyeFilled } from "@ant-design/icons";
import { Tooltip, Button } from "antd";

export const DeleteIcon = (props) => (
  <Tooltip title="Delete">
    <DeleteFilled onClick={props.onClick} />
  </Tooltip>
);

export const ViewIcon = (props) => (
  <Tooltip title="View">
    <EyeFilled onClick={props.onClick} />
  </Tooltip>
);
