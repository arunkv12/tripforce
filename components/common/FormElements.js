import {
  Form,
  Input,
  Select,
  DatePicker,
  Switch,
  InputNumber,
  Checkbox,
  Descriptions,
} from "antd";
import TextArea from "antd/lib/input/TextArea";
import moment from "moment";
import { useState } from "react";

const InputDefault = (props) => {
  return (
    <Form.Item
      hidden={props.hidden ? props.hidden : false}
      initialValue={props.initialValue != undefined ? props.initialValue : null}
      label={props.label ? props.label : ""}
      name={props.name}
      validateFirst={true}
      rules={[
        {
          pattern: props.pattern
            ? props.pattern
            : "^[a-zA-Z0-9-+#@,.()&/_: ]*$",
        },
        { required: props.required ? props.required : false },
        {
          min: props.min ? props.min : 3,
          max: props.max ? props.max : 100,
        },
      ]}
    >
      <Input
        placeholder={props.label}
        disabled={props.disabled}
        allowClear
        onChange={props.onChange}
        addonBefore={props.addonBefore}
      ></Input>
    </Form.Item>
  );
};

const InputTextNumber = (props) => {
  return (
    <Form.Item
      label={props.label}
      name={props.name}
      validateFirst={true}
      rules={[
        {
          pattern: props.pattern ? props.pattern : "^[0-9]*$",
        },
        { required: props.required ? props.required : false },
        {
          min: props.min ? props.min : 1,
          max: props.max ? props.max : 30,
        },
      ]}
    >
      <Input placeholder={props.label} allowClear></Input>
    </Form.Item>
  );
};

const InputNumberStepper = (props) => {
  return (
    <Form.Item
      initialValue={props.initialValue != undefined ? props.initialValue : null}
      label={props.label}
      name={props.name}
      validateFirst={true}
      rules={[{ required: props.required ? props.required : false }]}
    >
      <InputNumber
        disabled={props.disabled}
        style={{ width: "100%" }}
        min={props.min}
        max={props.max}
      />
    </Form.Item>
  );
};

const Email = (props) => {
  return (
    <Form.Item
      initialValue={props.initialValue != undefined ? props.initialValue : null}
      label={props.label != undefined ? props.label : "Email"}
      name={props.name != undefined ? props.name : "email"}
      rules={[
        {
          required: props.required === false ? props.required : true,
          type: "email",
        },
      ]}
    >
      <Input
        placeholder={props.label != undefined ? props.label : "Email"}
        disabled={props.disabled}
        allowClear
      ></Input>
    </Form.Item>
  );
};

const Phone = (props) => {
  return (
    <Form.Item
      validateFirst={true}
      label={props.label}
      name={props.name}
      rules={[
        { required: props.required ? props.required : false },
        {
          pattern: "(02|03|04|06|07|09)[0-9]{7}$",
        },
      ]}
    >
      <Input placeholder="02/03/04/06/07/09*****" allowClear></Input>
    </Form.Item>
  );
};

const Mobile = (props) => {
  return (
    <Form.Item
      validateFirst={true}
      initialValue={props.initialValue != undefined ? props.initialValue : null}
      label={props.label != undefined ? props.label : "Mobile"}
      name={props.name != undefined ? props.name : "mobile_no"}
      rules={[
        {
          required: props.required === false ? props.required : true,
          pattern: "(050|052|054|055|056|058)[0-9]{7}$",
        },
      ]}
    >
      <Input addonBefore="+91" allowClear></Input>
    </Form.Item>
  );
};

const InputTextAreaDefault = (props) => {
  return (
    <Form.Item
      initialValue={props.initialValue != undefined ? props.initialValue : null}
      label={props.label}
      name={props.name}
      validateFirst={true}
      rules={[
        {
          pattern: "^[a-zA-Z0-9-+#@,.()&/_: ]*$",
        },
        { required: props.required ? props.required : false },
        {
          min: props.min ? props.min : 3,
          max: props.max ? props.max : 400,
        },
      ]}
    >
      <TextArea
        allowClear
        placeholder={props.label}
        //autoSize={{ minRows: 3, maxRows: 6 }}
      ></TextArea>
    </Form.Item>
  );
};

const DropDown = (props) => {
  const { Option } = Select;
  const [optKey, setOptKey] = useState(
    props.optkey != undefined ? props.optkey : "key"
  );
  const [optValue, setOptValue] = useState(
    props.optvalue != undefined ? props.optvalue : "value"
  );

  return (
    <Form.Item
      extra={props.extra != undefined ? props.extra : null}
      initialValue={props.initialValue != undefined ? props.initialValue : null}
      validateFirst={true}
      label={props.label ? props.label : ""}
      name={props.name}
      tooltip={props.tooltip}
      rules={[
        {
          required: props.required ? props.required : false,
          message: "Please choose an item",
        },
      ]}
    >
      <Select
        disabled={props.disabled ? props.disabled : false}
        allowClear={props.required ? false : props.required}
        onChange={props.onChange}
        showSearch
        filterOption={(input, option) =>
          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
        placeholder={props.label}
      >
        {props.options != undefined
          ? props.options.map((item) => (
              <Option value={item[optKey]}>{item[optValue]}</Option>
            ))
          : ""}
      </Select>
    </Form.Item>
  );
};

const DateDefault = (props) => {
  const disabledData = (currentDate) => {
    if (props.type === "expiry") {
      return currentDate < moment().add(0, "days");
      //return currentDate < moment().startOf("day");
    } else {
      return false;
    }
  };
  return (
    <Form.Item
      validateFirst={true}
      label={props.label ? props.label : ""}
      name={props.name}
      rules={[
        {
          type: "object",
          required: props.required ? props.required : false,
        },
      ]}
    >
      <DatePicker
        allowClear
        format="DD-MMM-YYYY"
        disabledDate={disabledData}
        style={{ width: "100%" }}
        showToday={props.type === "expiry" ? false : true}
        placeholder={props.label}
      ></DatePicker>
    </Form.Item>
  );
};

const DateTimeDefault = (props) => {
  const disabledData = (currentDate) => {
    if (props.type === "expiry") {
      return currentDate < moment().add(0, "days");
      //return currentDate < moment().startOf("day");
    } else {
      return false;
    }
  };
  return (
    <Form.Item
      validateFirst={true}
      label={props.label ? props.label : ""}
      name={props.name}
      rules={[
        {
          type: "object",
          required: props.required ? props.required : false,
        },
      ]}
    >
      <DatePicker
        allowClear
        format="DD-MMM-YYYY HH:mm:ss"
        showTime
        disabledDate={disabledData}
        style={{ width: "100%" }}
        showToday={props.type === "expiry" ? false : true}
        placeholder={props.label}
      ></DatePicker>
    </Form.Item>
  );
};

const DateRangeDefault = (props) => {
  const { RangePicker } = DatePicker;
  const range = (start, end) => {
    const result = [];
    for (let i = start; i <= end; i++) {
      result.push(i);
    }
    return result;
  };

  const disabledData = (currentDate) => {
    if (props.type === "expiry") {
      return currentDate < moment().add(0, "days");
    } else {
      return currentDate < moment().startOf("day");
    }
  };

  const disabledTime = (dateSet, type) => {
    //console.log(date.day());
    /* if (dateSet != null) {
      console.log(dateSet.date());
    } */

    if (dateSet != null && moment().date() === dateSet.date()) {
      return {
        disabledHours: () => range(0, moment().hour()),
        disabledMinutes: () => range(0, moment().minute()),
        disabledSeconds: () => range(0, moment().second()),
      };
    }
  };
  /* const onCalendarChange = (dates, dateStrings, info) => {
    
    //console.log(dates);
    //console.log(dateStrings);
    //console.log(info);
  }; */
  return (
    <Form.Item
      validateFirst={true}
      label={props.label ? props.label : ""}
      name={props.name}
      rules={[
        {
          type: "array",
          required: props.required ? props.required : false,
        },
      ]}
    >
      <RangePicker
        allowClear
        format={props.showTime ? "DD-MMM-YYYY HH:mm:ss" : "DD-MMM-YYYY"}
        disabledDate={disabledData}
        disabledTime={disabledTime}
        style={{ width: "100%" }}
        showTime={props.showTime ? props.showTime : false}
        /* showTime={{
          hideDisabledOptions: true,
        }} */
        //showToday={props.type === "expiry" ? false : true}
        //placeholder={props.label}
        //onCalendarChange={onCalendarChange}
      ></RangePicker>
    </Form.Item>
  );
};

const SwitchLabelled = (props) => {
  return (
    <Form.Item
      initialValue={props.initialValue != undefined ? props.initialValue : null}
      required={props.required ? props.required : false}
      label={props.label}
      name={props.name}
      valuePropName="checked"
    >
      <Switch
        onChange={props.onChange}
        checkedChildren={props.checkedLabel}
        unCheckedChildren={props.unCheckedLabel}
      />
    </Form.Item>
  );
};

const CheckBoxDefault = (props) => {
  return (
    <Form.Item
      initialValue={props.initialValue != undefined ? props.initialValue : null}
      required={props.required ? props.required : false}
      name={props.name}
      valuePropName="checked"
    >
      <Checkbox>{props.label}</Checkbox>
    </Form.Item>
  );
};

const BooleanDescriptionItem = (props) => {
  return (
    <Descriptions.Item label={props.label1}>
      {props.value1 === "1" ? "Yes" : "No"}
    </Descriptions.Item>
  );
};

export {
  InputDefault,
  InputTextNumber,
  InputNumberStepper,
  Email,
  Mobile,
  Phone,
  InputTextAreaDefault,
  DropDown,
  DateDefault,
  DateTimeDefault,
  DateRangeDefault,
  SwitchLabelled,
  CheckBoxDefault,
  BooleanDescriptionItem,
};
