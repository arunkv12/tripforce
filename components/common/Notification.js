import { notification } from "antd";
export default function NotificationWrapped(type, title, description) {
  return notification[type]({
    message: title,
    description: description,
    duration: 30,
  });
}
