import { Table, Modal, Row } from "antd";
import { useState, useEffect } from "react";
import { async_get_with_pathparam as getwithpath } from "../../../api/gateway";
import url from "../../../api/url";
import { formatToDateTime } from "../../../util/date";

export default function Quotations(props) {
  const [dataSource, setDataSource] = useState([]);
  const [dataSourceLoading, setDataSourceLoading] = useState(true);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [quotationItems, setQuotationItems] = useState([]);

  const customerCols = [
    {
      title: "No",
      dataIndex: "quotation_num",
      ellipsis: true,
      render: (text, record) => (
        <a
          onClick={() => {
            setIsModalVisible(true);
            setQuotationItems(record.tf_quotations_items);
          }}
        >
          {text}
        </a>
      ),
    },
    {
      title: "Created On",
      dataIndex: "created_on",
      ellipsis: true,
      render: (created_on) => {
        return formatToDateTime(created_on);
      },
    },
  ];

  const quotationItemsCols = [
    {
      title: "Item",
      dataIndex: "item",
      ellipsis: true,
    },
    {
      title: "Quantity",
      dataIndex: "quantity",
      ellipsis: true,
    },
    {
      title: "Rate",
      dataIndex: "rate",
      ellipsis: true,
    },
    {
      title: "Amount",
      dataIndex: "amount",
      ellipsis: true,
      render: (text, record) => record.quantity * record.rate,
    },
  ];

  useEffect(() => {
    setDataSourceLoading(true);

    getwithpath(
      { path1: props.data.trip_num },
      {
        apiurl: url.customer_trips_quotations,
      }
    ).then((response) => {
      if (response.status === 200) {
        setDataSource(response.data);
      }
      setDataSourceLoading(false);
    });
  }, []);

  const calculateTotalAmountDisplay = () => {
    //let quotationItems = quotationItems;
    let totalAmt = 0;

    quotationItems.map((item) => {
      totalAmt = totalAmt + Number(item.rate) * item.quantity;
    });

    return totalAmt;
  };

  return (
    <>
      <Table
        columns={customerCols}
        dataSource={dataSource}
        size="middle"
        loading={dataSourceLoading}
      ></Table>

      {isModalVisible && (
        <Modal
          centered
          title={quotationItems[0].quotation_num}
          visible={isModalVisible}
          footer={null}
          onCancel={() => setIsModalVisible(false)}
        >
          <Table
            pagination={false}
            columns={quotationItemsCols}
            dataSource={quotationItems}
            size="middle"
          ></Table>

          <Row justify="end" style={{ marginTop: "24px" }}>
            <h2>Total &nbsp; &nbsp; ₹{calculateTotalAmountDisplay()}</h2>
          </Row>
        </Modal>
      )}
    </>
  );
}
