import { Row, Col, Space } from "antd";
import { FacebookFilled, LinkedinFilled } from "@ant-design/icons";

const SocialLinks = () => {
  return (
    <Space>
      <a
        target="_blank"
        href="https://www.facebook.com/Trip-Force-107110541670850 "
      >
        <FacebookFilled
          style={{ fontSize: "30px" }}
          className="color-white"
        ></FacebookFilled>
      </a>

      <a target="_blank" href="https://www.linkedin.com/company/tripforce">
        <LinkedinFilled
          style={{ fontSize: "30px" }}
          className="color-white"
        ></LinkedinFilled>
      </a>
    </Space>
  );
};
export default function Footer() {
  return (
    <Row justify="center" className="bg-primary h-ln-64">
      <Col xs={22} md={11}>
        <h3 className="color-white margin-0px">
          &copy; 2020-2021 TripForce All rights reserved
        </h3>
      </Col>

      <Col xs={0} md={11}>
        <Row justify="end" align="middle">
          <SocialLinks />
        </Row>
      </Col>

      <Col xs={22} md={0}>
        <Row justify="center" align="middle">
          <SocialLinks />
        </Row>
      </Col>
    </Row>
  );
}
