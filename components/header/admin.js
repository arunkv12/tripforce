import { Col, Menu, Row, Layout, Drawer } from "antd";
const { Header, Content, Footer } = Layout;
import Link from "next/link";
import { AlignRightOutlined } from "@ant-design/icons";
import { useState } from "react";

export default function HeaderIndex() {
  const [showDrawer, setShowDrawer] = useState(false);

  const MenuItems = ({ mode }) => {
    return (
      <Menu mode={mode}>
        <Menu.Item key="1">
          <a href="/admin/dashboard">
            <strong>DASHBOARD</strong>
          </a>
        </Menu.Item>
        <Menu.Item key="2">
          <a href="/admin/trips">
            <strong>TRIPS</strong>
          </a>
        </Menu.Item>
        <Menu.Item key="3">
          <strong>MY PROFILE</strong>
        </Menu.Item>
      </Menu>
    );
  };

  return (
    <Header
      className="app-header-padding box-shadow"
      style={{ backgroundColor: "white" }}
    >
      <Row justify="space-between">
        <Col xs={20} md={12}>
          <a href="/admin/dashboard">
            <img width="150px" src="/logo.jpeg"></img>
          </a>
        </Col>
        <Col xs={0} md={12}>
          <Row justify="end">
            <MenuItems mode="horizontal" />
          </Row>
        </Col>

        <Col xs={4} md={0}>
          <Row justify="end" align="middle" style={{ height: "100%" }}>
            <AlignRightOutlined
              className="hover-curser-pointer"
              style={{ fontSize: "25px" }}
              onClick={() => setShowDrawer(true)}
            />
          </Row>

          <Drawer
            height="100vh"
            placement="top"
            closable={true}
            visible={showDrawer}
            onClose={() => setShowDrawer(false)}
          >
            <MenuItems mode="vertical" />
          </Drawer>
        </Col>
      </Row>
    </Header>
  );
}
