import { Row, Col, List, Card } from "antd";

import {
  GlobalOutlined,
  TrophyFilled,
  ShoppingCartOutlined,
  CustomerServiceFilled,
  CrownOutlined,
  HeartFilled,
} from "@ant-design/icons";

export default function BookWithUs() {
  const data = [
    {
      title: "1",
      icon: <TrophyFilled style={{ fontSize: "60px", color: "white" }} />,
      feature_name: "Great Deal",
      feature_desc:
        "Great value tour and travel packages. Flexible transfers and private transportation",
    },
    {
      title: "2",
      icon: (
        <ShoppingCartOutlined style={{ fontSize: "60px", color: "white" }} />
      ),
      feature_name: "Fast Booking",
      feature_desc:
        "Expert assistance and wide network of quality travel suppliers",
    },
    {
      title: "1",
      icon: <GlobalOutlined style={{ fontSize: "60px", color: "white" }} />,
      feature_name: "Diverse Itineries",
      feature_desc:
        "Wide collection and coverage of different destinations as package",
    },
    {
      title: "1",
      icon: (
        <CustomerServiceFilled style={{ fontSize: "60px", color: "white" }} />
      ),
      feature_name: "Support Team",
      feature_desc:
        "24/7 customer support. Through verification and periodic updates of travel suppliers",
    },
    {
      title: "2",
      icon: <CrownOutlined style={{ fontSize: "60px", color: "white" }} />,
      feature_name: "Beautiful Destinations",
      feature_desc:
        "Best attractions at your fingertips. Wide collection of tourist breaks",
    },
    {
      title: "2",
      icon: <HeartFilled style={{ fontSize: "60px", color: "white" }} />,
      feature_name: "Passionate Traveller",
      feature_desc:
        "Passionate about travel, holidays explore the tour packages",
    },
  ];
  return (
    <div
      style={{
        minHeight: "100vh",
        //backgroundColor: "#ff8400",
        background: "linear-gradient(to right, #fe8c00, #f83600)",
        /* backgroundImage:
          "radial-gradient( circle 324px at 0.8% 51.7%,  rgba(234,92,125,1) 0%, rgba(236,112,102,1) 90% )", */
      }}
    >
      <Row
        align="middle"
        className="app-padding"
        style={{
          minHeight: "inherit",
        }}
      >
        <Col xs={24} md={10}>
          <h1 style={{ fontSize: "50px", color: "white", textAlign: "center" }}>
            Why Book with Us?
          </h1>
        </Col>
        <Col xs={24} md={14}>
          <List
            //grid={{ gutter: 24, column: 2 }}
            grid={{
              gutter: 16,
              column: 2,
              xs: 1,
              md: 2,
            }}
            dataSource={data}
            renderItem={(item) => (
              <List.Item>
                <Card
                  className="features-card"
                  bordered={false}
                  style={{ backgroundColor: "transparent !important" }}
                >
                  <Row justify="space-between">
                    <Col span={6}>{item.icon}</Col>
                    <Col span={18}>
                      <h1>{item.feature_name}</h1>
                      <h3 style={{ fontWeight: "200" }}>{item.feature_desc}</h3>
                    </Col>
                  </Row>
                </Card>
              </List.Item>
            )}
          />
        </Col>
      </Row>
    </div>
  );
}
