import { Row, Col, List, Card } from "antd";
const { Meta } = Card;

export default function Package() {
  const data = [
    {
      title: "1",
      package_name: "Sabarimala",
      package_count: "4",
    },
    {
      title: "2",
      package_name: "Thekkadi",
      package_count: "1",
    },
    {
      title: "1",
      package_name: "Velamkanni",
      package_count: "2",
    },
    {
      title: "1",
      package_name: "Kumarakom",
      package_count: "5",
    },
    {
      title: "2",
      package_name: "Munnar",
      package_count: "7",
    },
    {
      title: "1",
      package_name: "Kozhikode",
      package_count: "1",
    },
    {
      title: "1",
      package_name: "Tirupur",
      package_count: "3",
    },
    {
      title: "2",
      package_name: "Madhura",
      package_count: "1",
    },
    {
      title: "2",
      package_name: "Madhura",
      package_count: "1",
    },
  ];

  return (
    <div className="app-padding">
      <Row justify="center">
        <Col xs={24} md={18}>
          <h1
            style={{ textAlign: "center", fontSize: "40px", fontWeight: "200" }}
          >
            Visit hundreds of travel destination using{" "}
            <strong>tripforce</strong> with irresistable itineraries
          </h1>
        </Col>

        <Col xs={24} md={24}>
          <List
            grid={{
              gutter: 24,
              column: 3,
              xs: 1,
              md: 3,
            }}
            //grid={{ gutter: 24, column: 4 }}
            dataSource={data}
            renderItem={(item) => (
              <List.Item>
                <Card
                  hoverable
                  //style={{ width: 280 }}
                  cover={
                    <img
                      //alt="example"
                      src={"/package-" + item.title + ".jpg"}
                    />
                  }
                >
                  <Meta
                    title={item.package_name}
                    description={item.package_count + " Packages available"}
                  />
                </Card>
              </List.Item>
            )}
          />
        </Col>
      </Row>
    </div>
  );
}
