import {
  Col,
  Row,
  Input,
  Form,
  Button,
  Select,
  DatePicker,
  Alert,
  AutoComplete,
} from "antd";
import { useState, useEffect } from "react";
import { async_get as get, async_post as post } from "../../api/gateway";
import url from "../../api/url";
import moment from "moment";
import { getDateString } from "../../util/date";

export default function QuickBooking() {
  const { Option } = AutoComplete;

  const [tripPoints, setTripPoints] = useState([]);
  const [bookingForm] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [bookingRes, setBookingRes] = useState(false);
  const [bookingNum, setBookingNum] = useState(0);

  useEffect(() => {
    /* get({ apiurl: url.masters, group_code: "tp" }).then((response) => {
      if (response.status === 200) {
        setTripPoints(response.data);
      }
    }); */
  }, []);

  const onSearchTripPoints = (value) => {
    if (value != undefined && value.length > 0) {
      get({ apiurl: url.masters_trip_points, input: value }).then(
        (response) => {
          if (response.status === 200) {
            setTripPoints(response.data);
          }
        }
      );
    }
  };

  const saveBooking = (values) => {
    values.trip_end_pt_desc = values.trip_end_pt;
    values.trip_end_pt = tripPoints.filter(
      (item) => item.description === values.trip_end_pt
    )[0].place_id;

    values.contact_mobile_country_cd =
      values.contact_mobile_num != undefined &&
      values.contact_mobile_num.length > 0
        ? "+91"
        : "";

    values.trip_start_date = getDateString(values.trip_start_date);
    values.contact_first_name = values.contact_email;
    values.contact_last_name = values.contact_email;
    values.trip_type = "qb";
    let params = {};
    params.apiurl = url.bookings;

    setLoading(true);
    post(values, params).then((response) => {
      setLoading(false);
      if (response.status === 201) {
        setBookingRes(true);
        bookingForm.resetFields();
        setBookingNum(response.data.trip_num);
      } else {
        setBookingRes(false);
        setBookingNum(0);
      }
    });
  };

  return (
    <div className="booknow app-padding">
      <Row>
        <Col xs={0} md={12}>
          <img width="100%" src="/quick_bg.jpg"></img>
        </Col>
        <Col xs={24} md={12}>
          <div
            style={{ height: "100%", display: "flex", alignItems: "center" }}
          >
            <Row justify="center">
              <Col span={24}>
                <h1
                  style={{
                    textAlign: "center",
                    fontSize: "45px",
                    fontWeight: "200",
                  }}
                >
                  Now Let's make a Quick Booking
                </h1>
              </Col>

              <Col xs={22} md={20}>
                {bookingRes && (
                  <Alert
                    style={{ marginBottom: "10px" }}
                    message="Successfully submitted request!"
                    description={
                      <>
                        Booking Reference Number: <strong>{bookingNum}</strong>.
                        Please keep Reference Number for future communication.
                      </>
                    }
                    type="success"
                    showIcon
                    closable
                    onClose={(e) => setBookingRes(false)}
                  />
                )}
              </Col>

              <Col xs={22} md={20}>
                <Form
                  form={bookingForm}
                  size="large"
                  className="quickbook"
                  onFinish={saveBooking}
                >
                  <Form.Item
                    name="trip_end_pt"
                    rules={[
                      {
                        required: true,
                        message: "'Trip Point' is required",
                      },
                    ]}
                  >
                    <AutoComplete
                      allowClear
                      size="large"
                      filterOption={(input, option) =>
                        option.props.children
                          .toLowerCase()
                          .indexOf(input.toLowerCase()) >= 0
                      }
                      placeholder="Where would you like to go?"
                      onSearch={onSearchTripPoints}
                    >
                      {tripPoints.map((item) => (
                        <Option
                          key={item["place_id"]}
                          value={item["description"]}
                        >
                          {item["description"]}
                        </Option>
                      ))}
                    </AutoComplete>

                    {/* <Select
                      placeholder="Where would you like to go?"
                      allowClear
                      showSearch
                      size="large"
                      filterOption={(input, option) =>
                        option.props.children
                          .toLowerCase()
                          .indexOf(input.toLowerCase()) >= 0
                      }
                      onSearch={onSearchTripPoints}
                    >
                      {tripPoints.map((item) => (
                        <Option value={item["place_id"]}>
                          {item["description"]}
                        </Option>
                      ))}
                    </Select> */}
                  </Form.Item>
                  <Form.Item
                    name="trip_start_date"
                    rules={[
                      {
                        required: true,
                        message: "'Trip Date' is invalid",
                      },
                    ]}
                  >
                    <DatePicker
                      placeholder="When you like to go?"
                      format="YYYY-MM-DD"
                      disabledDate={(currentDate) =>
                        currentDate < moment().subtract(1, "days")
                      }
                      style={{ width: "100%" }}
                    />
                  </Form.Item>
                  <Form.Item
                    name="contact_mobile_num"
                    rules={[
                      {
                        required: true,
                        len: 10,
                        pattern: "^[0-9]+$",
                        message: "'Mobile' is invalid",
                      },
                    ]}
                  >
                    <Input
                      addonBefore="+91"
                      placeholder="Tell us your mobile number?"
                    />
                  </Form.Item>

                  <Form.Item
                    name="contact_email"
                    rules={[
                      {
                        required: true,
                        type: "email",
                        message: "'Email' is invalid",
                      },
                    ]}
                  >
                    <Input placeholder="Email" />
                  </Form.Item>
                  <Form.Item>
                    <Button
                      block
                      type="primary"
                      htmlType="submit"
                      loading={loading}
                    >
                      Submit
                    </Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  );
}
