module.exports = {
  trips:
    "SELECT tps.id, tps.trip_num, cus.customer_email, cus.first_name as customer_first_name, " +
    " cus.last_name as customer_last_name, " +
    " cus.mobile_country_cd as customer_mobile_country_cd, cus.mobile_num as customer_mobile_num, cus.verified as customer_verified, " +
    " tt.reference_desc_en as trip_type_desc, tps.trip_type," +
    " tps.trip_start_date, tps.trip_end_date, tps.trip_start_pt, tps.trip_start_pt_desc, " +
    " tps.trip_end_pt, tps.trip_end_pt_desc, tps.trip_transfer_points, tps.trip_transfer_points_desc, " +
    " tps.head_count, tps.vehicle_type, vt.reference_desc_en as vehicle_type_desc, tps.vehicle_count, tps.remarks, tps.contact_email, " +
    " tps.contact_mobile_country_cd, tps.contact_mobile_num, tps.contact_first_name, tps.contact_last_name, tps.created_on " +
    " FROM public.tf_trips tps " +
    " join tf_mst_data tt on tt.reference_code=tps.trip_type " +
    " join tf_customers cus on cus.customer_email=tps.customer_email " +
    " left join tf_mst_data vt on vt.reference_code=tps.vehicle_type",
};
