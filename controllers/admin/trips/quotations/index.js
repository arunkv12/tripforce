var express = require("express");
var quotationsRouter = express.Router();
const moment = require("moment");
const pool = require("../../../../pg/config");
var { prisma } = require("../../../../prisma/client");
const {
  generateIdByPkey,
  getHashTripNum,
} = require("../../../../helpers/functions");
const { sendMail } = require("../../../../helpers/sendgridmailer");

quotationsRouter
  .route("/:trip_num")
  .get(async function (req, res) {
    //console.log(req.params);
    try {
      let quotations = await prisma.tf_quotations.findMany({
        where: {
          trip_num: req.params.trip_num,
        },
        include: {
          tf_quotations_items: true,
        },
        orderBy: {
          created_on: "desc",
        },
      });

      //console.log(quotations);
      //console.log(toJson(quotations));
      res.status(200).json(quotations);
    } catch (error) {
      console.log(error);
      res.status(500).json(error);
    }
  })
  .post(async function (req, res) {
    try {
      let quotationId = await (
        await pool.query("select nextval('tf_quotations_id_seq')")
      ).rows[0].nextval;

      await prisma.tf_quotations.create({
        data: {
          //quotation
          id: Number(quotationId),
          quotation_num: "Q-" + generateIdByPkey(quotationId),
          status: 1,
          created_on: moment()
            .add(moment().utcOffset() / 60, "h")
            .format(),
          created_by: req.user.user_email,
          tf_quotations_items: {
            create:
              //quotation items
              req.body.items,
          },
          //connect to trips
          tf_trips: {
            connect: {
              trip_num: req.params.trip_num,
            },
          },
        },
      });

      await prisma.tf_records.create({
        data: {
          type: "new_quotation",
          record: "new quotation created",
          read_status: 0,
          target_to: "customer",
          status: 1,
          created_on: moment()
            .add(moment().utcOffset() / 60, "h")
            .format(),
          created_by: req.user.user_email,

          //connect to trips
          tf_trips: {
            connect: {
              trip_num: req.params.trip_num,
            },
          },
        },
      });

      //console.log(getHashTripNum(req.params.trip_num));
      //send mail notification to customer
      sendMail({
        type: "update",
        dynamic_url:
          req.headers.host +
          "/customer/trips/" +
          getHashTripNum(req.params.trip_num),
        to: req.body.contact_email,
        trip_num: req.params.trip_num,
      });

      res.sendStatus(201);
    } catch (error) {
      console.log(error);
      res.status(500).json(error);
    }
  });

module.exports = quotationsRouter;
