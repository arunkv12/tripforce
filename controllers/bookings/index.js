var express = require("express");
var bookingsRouter = express.Router();
var { prisma } = require("../../prisma/client");
var moment = require("moment");
var { sendMail } = require("../../helpers/sendgridmailer");
const { getHashTripNum, generateIdByPkey } = require("../../helpers/functions");
const pool = require("../../pg/config");

bookingsRouter
  .route("/")
  .get(async function (req, res) {
    const trips = await prisma.tf_trips.findMany();
    res.status(200).json(trips);
  })
  .post(async function (req, res) {
    //check customer exists with email id, if yes add trip data.
    //else create new customer and trip data
    try {
      const customer = await prisma.tf_customers.findUnique({
        where: {
          customer_email: req.body.contact_email,
        },
      });
      let resp;

      let tripId = await (await pool.query("select nextval('tf_trips_id_seq')"))
        .rows[0].nextval;

      if (customer != null) {
        resp = await prisma.tf_trips.create({
          data: {
            id: Number(tripId),
            trip_num: "T-" + generateIdByPkey(tripId),
            customer_email: req.body.contact_email,
            trip_type: req.body.trip_type,
            edit_lock: 0,
            trip_start_date: new Date(req.body.trip_start_date),
            trip_end_date: new Date(req.body.trip_end_date),
            trip_start_pt: req.body.trip_start_pt,
            trip_start_pt_desc: req.body.trip_start_pt_desc,
            trip_end_pt: req.body.trip_end_pt,
            trip_end_pt_desc: req.body.trip_end_pt_desc,
            trip_transfer_points: req.body.trip_transfer_points,
            trip_transfer_points_desc: req.body.trip_transfer_points_desc,
            head_count: req.body.head_count,
            vehicle_type: req.body.vehicle_type,
            remarks: req.body.remarks,
            contact_email: req.body.contact_email,
            contact_mobile_country_cd: req.body.contact_mobile_country_cd,
            contact_mobile_num: req.body.contact_mobile_num,
            contact_first_name: req.body.contact_first_name,
            contact_last_name: req.body.contact_last_name,
            status: 1,
            created_on: moment()
              .add(moment().utcOffset() / 60, "h")
              .format(),
            created_by: req.body.contact_email,
          },
          select: { trip_num: true },
        });
      } else {
        resp = await prisma.tf_customers.create({
          data: {
            customer_email: req.body.contact_email,
            first_name: req.body.contact_first_name,
            last_name: req.body.contact_last_name,
            mobile_country_cd: req.body.contact_mobile_country_cd,
            mobile_num: req.body.contact_mobile_num,
            verified: 0,
            status: 1,
            created_on: moment()
              .add(moment().utcOffset() / 60, "h")
              .format(),
            created_by: req.body.contact_email,
            tf_trips: {
              create: {
                id: Number(tripId),
                trip_num: "T-" + generateIdByPkey(tripId),
                trip_type: req.body.trip_type,
                edit_lock: 0,
                trip_start_date:
                  req.body.trip_start_date != undefined
                    ? new Date(req.body.trip_start_date)
                    : null,
                trip_end_date: new Date(req.body.trip_end_date),
                trip_start_pt: req.body.trip_start_pt,
                trip_start_pt_desc: req.body.trip_start_pt_desc,
                trip_end_pt: req.body.trip_end_pt,
                trip_end_pt_desc: req.body.trip_end_pt_desc,
                trip_transfer_points: req.body.trip_transfer_points,
                trip_transfer_points_desc: req.body.trip_transfer_points_desc,
                head_count: req.body.head_count,
                vehicle_type: req.body.vehicle_type,
                remarks: req.body.remarks,
                contact_email: req.body.contact_email,
                contact_mobile_country_cd: req.body.contact_mobile_country_cd,
                contact_mobile_num: req.body.contact_mobile_num,
                contact_first_name: req.body.contact_first_name,
                contact_last_name: req.body.contact_last_name,
                status: 1,
                created_on: moment()
                  .add(moment().utcOffset() / 60, "h")
                  .format(),
                created_by: req.body.contact_email,
              },
            },
          },
          include: {
            tf_trips: true,
          },
        });

        resp = { trip_num: resp.tf_trips[0].trip_num };
      }

      //send mail notification to customer
      sendMail({
        type: "new",
        dynamic_url:
          req.headers.host +
          "/customer/trips/" +
          getHashTripNum(resp.trip_num.toString()),
        to: req.body.contact_email,
        trip_num: resp.trip_num,
      });

      res.status(201).json(resp);
    } catch (error) {
      console.log(error);
      res.status(500).json(error);
    }
  });

//post

module.exports = bookingsRouter;
