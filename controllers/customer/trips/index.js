var express = require("express");
var tripsRouter = express.Router();
const { trips } = require("../../admin/trips/query");
const { decryptHashTripNum } = require("../../../helpers/functions");
const pool = require("../../../pg/config");

tripsRouter
  .route("/:trip_num")
  .get(async function (req, res) {
    let tripNum = decryptHashTripNum(req.params.trip_num);
    if (tripNum.length > 0) {
      let query = trips + " where tps.trip_num='" + tripNum + "'";

      pool.query(query, (error, result) => {
        if (error) {
          //console.log(console.error);
          res.status(500).json({ error_msg: error });
        } else {
          if (result.rows.length > 0) {
            let tripDetails = {};
            tripDetails.contact_email = result.rows[0].contact_email;
            tripDetails.contact_first_name = result.rows[0].contact_first_name;
            tripDetails.contact_last_name = result.rows[0].contact_last_name;
            tripDetails.contact_mobile_country_cd =
              result.rows[0].contact_mobile_country_cd;
            tripDetails.contact_mobile_num = result.rows[0].contact_mobile_num;

            tripDetails.head_count = result.rows[0].head_count;
            tripDetails.id = result.rows[0].id;
            tripDetails.trip_type = result.rows[0].trip_type;
            tripDetails.remarks = result.rows[0].remarks;
            tripDetails.trip_end_date = result.rows[0].trip_end_date;
            tripDetails.trip_end_pt_desc = result.rows[0].trip_end_pt_desc;
            tripDetails.trip_num = result.rows[0].trip_num;
            tripDetails.trip_start_date = result.rows[0].trip_start_date;
            tripDetails.trip_start_pt_desc = result.rows[0].trip_start_pt_desc;
            tripDetails.trip_transfer_points_desc =
              result.rows[0].trip_transfer_points_desc;
            tripDetails.vehicle_count = result.rows[0].vehicle_count;
            tripDetails.vehicle_type_desc = result.rows[0].vehicle_type_desc;
            res.status(200).json(tripDetails);
          } else {
            res.sendStatus(204);
          }
        }
      });
    } else {
      res.send(400).send("Bad Request");
    }
  })
  .post(async function (req, res) {});

//post

module.exports = tripsRouter;
