var express = require("express");
var quotationsRouter = express.Router();
var { prisma } = require("../../../../prisma/client");

quotationsRouter.route("/:trip_num").get(async function (req, res) {
  try {
    let quotations = await prisma.tf_quotations.findMany({
      where: {
        trip_num: req.params.trip_num,
      },
      include: {
        tf_quotations_items: true,
      },
      orderBy: {
        created_on: "desc",
      },
    });

    res.status(200).json(quotations);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

module.exports = quotationsRouter;
