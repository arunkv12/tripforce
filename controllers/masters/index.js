var express = require("express");
var mastersRouter = express.Router();
var { prisma } = require("../../prisma/client");
const { Client } = require("@googlemaps/google-maps-services-js");

mastersRouter
  .route("/")
  .get(async function (req, res) {
    const masters = await prisma.tf_mst_data.findMany({
      where: {
        group_code: req.query.group_code,
      },
    });
    res.status(200).json(masters);
  })
  .post(async function (req, res) {});

mastersRouter.route("/trip_points").get(function (req, res) {
  const client = new Client({});

  client
    .placeAutocomplete({
      params: {
        input: req.query.input,
        key: process.env.GOOGLE_MAPS_API_KEY,
        components: "country:in",
        types: "(cities)",
      },
      //timeout: 1000,
    })
    .then((r) => {
      if (r.data.status === "OK") {
        res.status(200).json(r.data.predictions);
      } else if (r.data.status === "ZERO_RESULTS") {
        res.sendStatus(204);
      } else {
        res.status(500).json(r.data.status);
      }

      //console.log(r.data.predictions);
    })
    .catch((e) => {
      console.log(e);
      res.status(500).json(e.response.data);
    });
});

module.exports = mastersRouter;
