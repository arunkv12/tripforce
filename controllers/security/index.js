var jwt = require("jsonwebtoken");
var { prisma } = require("../../prisma/client");

const authenticate = async function (req, res) {
  try {
    const users = await prisma.tf_users.findMany({
      where: {
        user_email: req.body.user_email,
        user_pwd: req.body.user_pwd,
      },
    });

    if (users.length > 0) {
      let user = users[0];
      delete user.user_pwd;
      user.token = jwt.sign(user, process.env.JWT_SECRET, {
        expiresIn: "1d",
      });

      res.status(200).json(user);
    } else {
      res.sendStatus(400);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
};

const authorize = function (req, res, next) {
  //console.log("hit authenticate token");
  const token = req.headers["authorization"];
  if (token == null) res.sendStatus(401);

  jwt.verify(token, process.env.JWT_SECRET, (error, user) => {
    //console.log(error);
    if (error) return res.sendStatus(401);
    req.user = user;
    next();
  });
};

module.exports = { authenticate, authorize };
