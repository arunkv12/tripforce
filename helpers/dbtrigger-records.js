const pool = require("../pg/config");
console.log("listener started......");
pool.query("LISTEN tf_records_insert");
pool.on("notification", async (data) => {
  console.log("a new row inserted in tf_records \n");
  console.log("-----------------------------------------");
  console.log(JSON.parse(data.payload));
  console.log("-----------------------------------------");
});
