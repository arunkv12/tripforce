const moment = require("moment");
const crypto = require("crypto");

const generateIdByPkey = function generateIdByPkey(id) {
  return id + moment().format("YYYYMMDD") + Math.floor(Math.random() * 100) + 1;
};

const toJson = function toJson(data) {
  return JSON.stringify(data, (key, value) =>
    typeof value === "bigint" ? value.toString() : value
  );
};

const getHashTripNum = function getHashTripNum(tripNum) {
  var mykey = crypto.createCipher("aes-128-cbc", "tripfoce*$3214key");
  var mystr = mykey.update(tripNum, "utf8", "hex");
  return (mystr += mykey.final("hex"));
};

const decryptHashTripNum = function decryptHashTripNum(hashTripNum) {
  var mykey = crypto.createDecipher("aes-128-cbc", "tripfoce*$3214key");
  var mystr = "";
  try {
    mystr = mykey.update(hashTripNum, "hex", "utf8");
  } catch (e) {
    return mystr;
  }
  return (mystr += mykey.final("utf8"));
};

module.exports = {
  generateIdByPkey,
  toJson,
  getHashTripNum,
  decryptHashTripNum,
};
