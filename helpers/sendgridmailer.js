const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const content =
  "<div>Dear Customer, <table><tr><td></td></tr></table> <table><tr><td>##subject</td></tr></table> <table><tr><td>  <strong>Booking Reference Number is ##tripnum" +
  "</strong></td></tr></table> <table><tr><td></td></tr></table> " +
  "<table><tr><td></td></tr></table>" +
  '<table><tr><td><strong><a target="_blank" href="##dynamic_url">Click here to view details</a></strong></td></tr></table>' +
  "<table><tr><td></td></tr></table>" +
  "<table><tr><td></td></tr></table>" +
  "<table><tr><td></td></tr></table><table><tr><td></td></tr></table><table><tr><td>Regards,</td></tr></table><table><tr><td>Team TripForce.</td></tr></table></div>";

const sendMail = (msg) => {
  let contentData = content;
  contentData = contentData.replace(
    "##subject",
    msg.type === "new"
      ? "Your enquiry with TripForce has been received."
      : "Your enquiry with TripForce has an update."
  );
  contentData = contentData.replace("##tripnum", msg.trip_num);
  contentData = contentData.replace("##dynamic_url", msg.dynamic_url);

  msg.from = process.env.SENDGRID_MAIL;
  msg.subject =
    msg.type === "new" ? "Hello from TripForce" : "An Update from TripForce";
  msg.html = contentData;

  sgMail
    .send(msg)
    .then((response) => {
      //console.log(response);
    })
    .catch((error) => {
      console.error(error);
    });
};

exports.sendMail = sendMail;
