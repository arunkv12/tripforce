import AdminHeader from "../components/header/admin";

const AdminLayout = (props) => (
  <div className="app-maxwidth">
    <AdminHeader></AdminHeader>
    <div className="grey-bg-fullscr-header">{props.children}</div>
  </div>
);

export default AdminLayout;
