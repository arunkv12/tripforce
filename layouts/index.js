import Header from "../components/header";
import Footer from "../components/footer";

const Layout = (props) => (
  <div className="app-maxwidth">
    <Header />
    <div>{props.children}</div>
    <Footer />
  </div>
);

export default Layout;
