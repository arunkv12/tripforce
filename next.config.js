const fs = require("fs");
const path = require("path");
const lessToJS = require("less-vars-to-js");

const themeVariables = lessToJS(
  fs.readFileSync(path.resolve(__dirname, "./assets/styles.less"), "utf8")
);

const withLess = require("@zeit/next-less");
nextConfig = {
  distDir: "build",
  env: {
    api: "/api",
  },
  onDemandEntries: {
    maxInactiveAge: 1000 * 60 * 60,
    pagesBufferLength: 5,
  },
  lessLoaderOptions: {
    javascriptEnabled: true,
    modifyVars: themeVariables,
  },
  //webpack: (config) => config,
  webpack: (config, { isServer }) => {
    if (isServer) {
      const antStyles = /antd\/.*?\/style.*?/;
      const origExternals = [...config.externals];
      config.externals = [
        (context, request, callback) => {
          if (request.match(antStyles)) return callback();
          if (typeof origExternals[0] === "function") {
            origExternals[0](context, request, callback);
          } else {
            callback();
          }
        },
        ...(typeof origExternals[0] === "function" ? [] : origExternals),
      ];
    }
    return config;
  },
};

module.exports = withLess(nextConfig);
