import Layout from "../../layouts/admin";
import { Col, List, Card, Row } from "antd";
import { MailFilled } from "@ant-design/icons";

export default function Dashboard(props) {
  const { Meta } = Card;

  const data = [
    {
      type: "Open Trips",
      count: "10",
    },
    {
      type: "Closed Trips",
      count: "100",
    },
    {
      type: "Quotations",
      count: "50",
    },
  ];

  return (
    <Layout>
      <Col xs={24} md={24} className="app-padding">
        <h1
          style={{
            fontWeight: "300",
            fontSize: "35px",
          }}
        >
          Dashboard.
        </h1>

        <List
          grid={{
            gutter: 48,
            column: 3,
            xs: 1,
            md: 3,
          }}
          //grid={{ gutter: 24, column: 4 }}
          dataSource={data}
          renderItem={(item) => (
            <List.Item>
              <Card
                hoverable
                //style={{ width: 280 }}
                /* cover={
                  <img
                    //alt="example"
                    src={"/package-" + item.title + ".jpg"}
                  />
                } */
              >
                <Meta title={item.type} description={item.count} />
              </Card>
            </List.Item>
          )}
        />
      </Col>
    </Layout>
  );
}
