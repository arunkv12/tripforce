import { Form, Col, Row, Button, Input, message } from "antd";
import { useRouter } from "next/router";
import { async_post as post } from "../../api/gateway";
import url from "../../api/url";

export default function Admin(props) {
  const router = useRouter();

  const login = (values) => {
    post(values, { apiurl: url.users_auth }).then((response) => {
      if (response.status === 200) {
        message.success("Login success!");
        localStorage.setItem("app_token", response.data.token);
        router.push("/admin/dashboard");
      } else {
        message.error("Login failed, check credentails and try again!");
      }
    });
  };

  return (
    <Row justify="center" align="middle" className="grey-bg-fullscr">
      <Col
        xs={24}
        md={7}
        className="box-shadow"
        style={{
          backgroundColor: "white",
          padding: "40px",
          borderRadius: "3px",
        }}
      >
        <Form size="large" layout="vertical" onFinish={login}>
          <div style={{ textAlign: "center", marginBottom: "20px" }}>
            <a href="/">
              <img width="180px" src="/logo.jpeg"></img>
            </a>
          </div>

          <Form.Item
            label="Email"
            name="user_email"
            rules={[{ required: true, message: "Please input your email!" }]}
          >
            <Input type="email" />
          </Form.Item>

          <Form.Item
            label="Password"
            name="user_pwd"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item>
            <Button block type="primary" htmlType="submit">
              Login
            </Button>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
}
