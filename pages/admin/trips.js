import Layout from "../../layouts/admin";
import {
  Col,
  List,
  Card,
  Row,
  Table,
  Form,
  Button,
  Tabs,
  Drawer,
  Descriptions,
  Space,
} from "antd";
import { async_post as post, async_get as get } from "../../api/gateway";
import url from "../../api/url";
import { useEffect, useState } from "react";
import { formatToDate } from "../../util/date";
import {
  DateRangeDefault,
  DropDown,
  InputDefault,
} from "../../components/common/FormElements";
import MoreInfo from "../../components/admin/trips/MoreInfo";
import Quotations from "../../components/admin/trips/Quotations";
import { DeleteIcon, ViewIcon } from "../../components/common/Button_Icons";
import { PlusOutlined } from "@ant-design/icons";
import AddEditTrip from "../../components/admin/trips/AddEditTrip";

export default function Trips(props) {
  const { TabPane } = Tabs;

  const [loading, setLoading] = useState(true);
  const [reload, setReload] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const [openDrawer, setOpenDrawer] = useState(false);
  const [viewDetails, setViewDetails] = useState(false);
  const [tripData, setTripData] = useState({});

  const columns = [
    {
      title: "Trip No",
      dataIndex: "trip_num",
      ellipsis: true,
      render: (text, record) => (
        <a onClick={() => onClickView(record)}>{text}</a>
      ),
    },
    {
      title: "Customer Email",
      dataIndex: "customer_email",
      ellipsis: true,
    },
    {
      title: "Type",
      dataIndex: "trip_type_desc",
      ellipsis: true,
    },
    {
      title: "Trip Start Date",
      dataIndex: "trip_start_date",
      ellipsis: true,
      render: (trip_start_date) => {
        return formatToDate(trip_start_date);
      },
    },
    {
      title: "Trip End Date",
      dataIndex: "trip_end_date",
      ellipsis: true,
      render: (trip_end_date) => {
        return formatToDate(trip_end_date);
      },
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Space>
          <ViewIcon onClick={() => onClickView(record)} />
          <DeleteIcon />
        </Space>
      ),
    },
  ];

  useEffect(() => {
    get({ apiurl: url.admin_trips }).then((response) => {
      setLoading(false);
      if (response.status === 200) {
        setDataSource(response.data);
      }
    });
  }, [reload]);

  const onClickView = (record) => {
    setViewDetails(true);
    setOpenDrawer(true);
    setTripData(record);
  };

  const closeDrawer = () => {
    setOpenDrawer(false);
    setViewDetails(false);
    setReload(!reload);
  };

  const onClickAdd = () => {
    setOpenDrawer(true);
  };
  return (
    <Layout>
      <Col xs={24} md={24} className="app-padding">
        <Row align="middle">
          <h1
            style={{
              fontWeight: "300",
              fontSize: "35px",
            }}
          >
            Trips.
            {/* <PlusCircleOutlined
            onClick={() => onClickAdd()}
            style={{ fontSize: "25px", marginLeft: "5px" }}
          /> */}
          </h1>
          <Button
            style={{ marginLeft: "5px", marginBottom: "25px" }}
            size="small"
            icon={<PlusOutlined />}
            onClick={() => onClickAdd()}
          >
            Add
          </Button>
        </Row>

        <Form layout="horizontal" size="large">
          <Row gutter={16}>
            <Col xs={24} md={7}>
              <DateRangeDefault label="Request Time"></DateRangeDefault>
            </Col>
            <Col xs={24} md={4}>
              <DropDown label="Type"></DropDown>
            </Col>
            <Col xs={24} md={6}>
              <InputDefault label="Customer Email"></InputDefault>
            </Col>
            <Col xs={24} md={5}>
              <InputDefault label="Trip No"></InputDefault>
            </Col>
            <Col xs={24} md={2}>
              <Form.Item>
                <Button block type="primary">
                  Search
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>

        <Table
          columns={columns}
          dataSource={dataSource}
          size="middle"
          loading={loading}
        ></Table>

        {openDrawer && (
          <Drawer
            title={null}
            placement="bottom"
            closable={true}
            onClose={() => closeDrawer()}
            visible={openDrawer}
            height="90vh"
            //key={placement}
          >
            {viewDetails ? (
              <Tabs defaultActiveKey="1" centered>
                <TabPane tab="Trip Details" key="1">
                  <MoreInfo data={tripData} />
                </TabPane>
                <TabPane tab="Quotations" key="2">
                  <Quotations data={tripData} />
                </TabPane>
              </Tabs>
            ) : (
              <Tabs defaultActiveKey="1" centered>
                <TabPane tab="Add a Trip" key="1">
                  <AddEditTrip mode="add" closeDrawer={closeDrawer} />
                </TabPane>
              </Tabs>
            )}
          </Drawer>
        )}
      </Col>
    </Layout>
  );
}
