import {
  Button,
  Card,
  Space,
  Col,
  Row,
  Input,
  DatePicker,
  Form,
  Select,
  InputNumber,
  Result,
  Alert,
  AutoComplete,
} from "antd";
import { useEffect, useState } from "react";
import Layout from "../layouts";
import { SendOutlined } from "@ant-design/icons";
import { useRouter } from "next/router";
import { async_get as get, async_post as post } from "../api/gateway";
import url from "../api/url";
import moment from "moment";
import { messages } from "../util/validations";
import { getDateTimeString, getDateString } from "../util/date";
import { HomeOutlined, RedoOutlined } from "@ant-design/icons";

export default function Home(props) {
  const router = useRouter();
  const { Option } = AutoComplete;

  const [formType, setFormType] = useState("");
  const [showQuoteOpt, setShowQuoteOpt] = useState(true);
  const { RangePicker } = DatePicker;
  const [tripStartPoints, setTripStartPoints] = useState([]);
  const [tripEndPoints, setTripEndPoints] = useState([]);
  const [tripTransferPoints, setTripTransferPoints] = useState([]);
  const [vehicleTypes, setVehicleType] = useState([]);
  const [bookingRes, setBookingRes] = useState(false);
  const [bookingNum, setBookingNum] = useState(0);
  const [bookingForm] = Form.useForm();

  useEffect(() => {
    //preferenceForm.setFieldsValue({ head_count: 2 });
    if (Object.keys(props).length > 2) {
      setTripEndPoints([
        { place_id: props.trip_end_pt, description: props.trip_end_pt_desc },
      ]);
      setShowQuoteOpt(false);
      setFormType("quote");
      bookingForm.setFieldsValue({
        trip_end_pt: props.trip_end_pt_desc,
        trip_date: [
          moment.unix(Number(props.trip_start_dt)),
          moment.unix(Number(props.trip_end_dt)),
        ],
      });
    }
  }, []);

  useEffect(() => {
    get({ apiurl: url.masters, group_code: "vt" }).then((response) => {
      if (response.status === 200) {
        setVehicleType(response.data);
      }
    });
  }, []);

  const onSearchTripPoints = (value, type) => {
    if (value != undefined && value.length > 0) {
      get({ apiurl: url.masters_trip_points, input: value }).then(
        (response) => {
          if (response.status === 200) {
            if (type === "start") {
              setTripStartPoints(response.data);
            } else if (type === "end") {
              setTripEndPoints(response.data);
            } else {
              setTripTransferPoints(response.data);
            }
          }
        }
      );
    }
  };

  const saveBooking = (values) => {
    //set start, end, transfer points desc

    if (formType === "quote") {
      values.trip_start_pt_desc = values.trip_start_pt;
      values.trip_start_pt = tripStartPoints.filter(
        (item) => item.description === values.trip_start_pt
      )[0].place_id;

      values.trip_end_pt_desc = values.trip_end_pt;
      values.trip_end_pt = tripEndPoints.filter(
        (item) => item.description === values.trip_end_pt
      )[0].place_id;

      if (
        values.trip_transfer_points != undefined &&
        values.trip_transfer_points.length > 0
      ) {
        let transferPoints = tripTransferPoints.filter((point) => {
          return values.trip_transfer_points.find((place_id) => {
            return point.place_id === place_id;
          });
        });

        values.trip_transfer_points_desc = transferPoints
          .map((point) => {
            let desc = point.description + "###";
            return desc;
          })
          .toString()
          .slice(0, -3);
        values.trip_transfer_points = values.trip_transfer_points.toString();
      } else {
        values.trip_transfer_points = "";
      }

      if (values.trip_date != undefined && values.trip_date.length > 0) {
        values.trip_start_date = getDateString(values.trip_date[0]);
        values.trip_end_date = getDateString(values.trip_date[1]);
      }
    }

    values.contact_mobile_country_cd =
      values.contact_mobile_num != undefined &&
      values.contact_mobile_num.length > 0
        ? "+91"
        : "";

    values.trip_type = formType === "quote" ? "rfq" : "rfc";
    values.booking_source = "customer";

    let params = {};
    params.apiurl = url.bookings;

    post(values, params).then((response) => {
      if (response.status === 201) {
        setBookingRes(true);
        bookingForm.resetFields();
        setBookingNum(response.data.trip_num);
        router.push("/booknow");
      } else {
        setBookingRes(false);
        setBookingNum(0);
      }
    });
  };

  const bookAgain = () => {
    setBookingRes(false);
    setBookingNum(0);
    bookingForm.resetFields();
    router.push("/booknow");
  };

  return (
    <Layout>
      <Form
        form={bookingForm}
        size="large"
        layout="horizontal"
        onFinish={saveBooking}
      >
        <div className="booknow app-padding app-bg-common">
          {bookingRes && (
            <Alert
              message="Successfully submitted request!"
              description={
                <>
                  Booking Reference Number is <strong>{bookingNum}</strong>.
                  Please keep Reference Number for future communication.
                </>
              }
              type="success"
              showIcon
              closable
              onClose={(e) => setBookingRes(false)}
            />
          )}
          <h1 className="h1-heading">Book Now.</h1>
          {showQuoteOpt && (
            <Card bordered={false} style={{ marginBottom: "40px" }}>
              <h2 style={{ marginBottom: "40px" }}>
                How do you want to get your qoute?
              </h2>
              <Row justify="space-between">
                <Col xs={24} md={11}>
                  <Button
                    style={{
                      whiteSpace: "break-spaces",
                      marginBottom: "20px",
                    }}
                    danger
                    block
                    type={formType === "quote" ? "primary" : "default"}
                    onClick={() => {
                      setFormType("quote");
                    }}
                  >
                    Request for Quote using the package selected
                  </Button>
                </Col>

                <Col xs={24} md={11}>
                  <Button
                    style={{ whiteSpace: "break-spaces" }}
                    danger
                    block
                    type={formType === "call" ? "primary" : "default"}
                    onClick={() => {
                      setFormType("call");
                    }}
                  >
                    Do you want us to call you to prepare a Quote
                  </Button>
                </Col>
              </Row>
            </Card>
          )}
          {formType === "quote" && (
            <>
              <Card bordered={false} style={{ marginBottom: "40px" }}>
                <h2 style={{ marginBottom: "40px" }}>Travel Plan</h2>

                <Col span={24}>
                  <Row gutter={16}>
                    <Col xs={24} md={5}>
                      <Form.Item
                        name="trip_start_pt"
                        rules={[
                          {
                            required: true,
                            message: "'Trip Start Point' is invalid",
                          },
                        ]}
                      >
                        <AutoComplete
                          allowClear
                          filterOption={(input, option) =>
                            option.props.children
                              .toLowerCase()
                              .indexOf(input.toLowerCase()) >= 0
                          }
                          placeholder="Type Trip Start Point"
                          onSearch={(value) =>
                            onSearchTripPoints(value, "start")
                          }
                        >
                          {tripStartPoints.map((item) => (
                            <Option
                              key={item["place_id"]}
                              value={item["description"]}
                            >
                              {item["description"]}
                            </Option>
                          ))}
                        </AutoComplete>

                        {/* <Select
                          placeholder="Choose Trip Start Point"
                          allowClear
                          showSearch
                          size="large"
                          filterOption={(input, option) =>
                            option.props.children
                              .toLowerCase()
                              .indexOf(input.toLowerCase()) >= 0
                          }
                          onSearch={(value) =>
                            onSearchTripPoints(value, "start")
                          }
                        >
                          {tripStartPoints.map((item) => (
                            <Option value={item["place_id"]}>
                              {item["description"]}
                            </Option>
                          ))}
                        </Select> */}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={5}>
                      <Form.Item
                        name="trip_end_pt"
                        rules={[
                          {
                            required: true,
                            message: "'Trip End Point' is required",
                          },
                        ]}
                      >
                        <AutoComplete
                          allowClear
                          filterOption={(input, option) =>
                            option.props.children
                              .toLowerCase()
                              .indexOf(input.toLowerCase()) >= 0
                          }
                          placeholder="Type Trip End Point"
                          onSearch={(value) => onSearchTripPoints(value, "end")}
                        >
                          {tripEndPoints.map((item) => (
                            <Option
                              key={item["place_id"]}
                              value={item["description"]}
                            >
                              {item["description"]}
                            </Option>
                          ))}
                        </AutoComplete>
                        {/* <Select
                          placeholder="Choose Trip End Point"
                          allowClear
                          showSearch
                          size="large"
                          filterOption={(input, option) =>
                            option.props.children
                              .toLowerCase()
                              .indexOf(input.toLowerCase()) >= 0
                          }
                          onSearch={(value) => onSearchTripPoints(value, "end")}
                        >
                          {tripEndPoints.map((item) => (
                            <Option value={item["place_id"]}>
                              {item["description"]}
                            </Option>
                          ))}
                        </Select> */}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={6}>
                      <Form.Item
                        name="trip_date"
                        rules={[
                          {
                            required: true,
                            message: "'Trip Dates' is invalid",
                          },
                        ]}
                      >
                        <RangePicker
                          disabledDate={(currentDate) =>
                            currentDate < moment().subtract(1, "days")
                          }
                          format="YYYY-MM-DD"
                          //defaultValue={[moment(), moment().add(1, "days")]}
                          style={{ padding: "14px" }}
                        />
                      </Form.Item>
                    </Col>

                    <Col xs={24} md={6}>
                      <Form.Item name="trip_transfer_points">
                        <Select
                          placeholder="Type Trip Transfer Points"
                          mode="multiple"
                          allowClear
                          showSearch
                          filterOption={(input, option) =>
                            option.props.children
                              .toLowerCase()
                              .indexOf(input.toLowerCase()) >= 0
                          }
                          onSearch={(value) =>
                            onSearchTripPoints(value, "transfer")
                          }
                        >
                          {tripTransferPoints.map((item) => (
                            <Option value={item["place_id"]}>
                              {item["description"]}
                            </Option>
                          ))}
                        </Select>
                      </Form.Item>
                    </Col>
                  </Row>
                </Col>
              </Card>
              <Card bordered={false} style={{ marginBottom: "40px" }}>
                <h2 style={{ marginBottom: "40px" }}>Travel Preference</h2>

                <Col span={24}>
                  <Row gutter={16}>
                    <Col xs={24} md={5}>
                      <Form.Item name="head_count">
                        <InputNumber
                          min={2}
                          max={100}
                          style={{ width: "100%" }}
                          placeholder="Number Of Passengers"
                        ></InputNumber>
                      </Form.Item>
                    </Col>

                    <Col xs={24} md={5}>
                      <Form.Item name="vehicle_type">
                        <Select
                          placeholder="Choose Vehicle Type"
                          allowClear
                          showSearch
                          filterOption={(input, option) =>
                            option.props.children
                              .toLowerCase()
                              .indexOf(input.toLowerCase()) >= 0
                          }
                        >
                          {vehicleTypes.map((item) => (
                            <Option value={item["reference_code"]}>
                              {item["reference_desc_en"]}
                            </Option>
                          ))}
                        </Select>
                      </Form.Item>
                    </Col>

                    <Col xs={24} md={5}>
                      <Form.Item name="remarks">
                        <Input placeholder="Any Special Remarks"></Input>
                      </Form.Item>
                    </Col>
                  </Row>
                </Col>
              </Card>
            </>
          )}
          {formType.length > 0 && (
            <Card bordered={false} style={{ marginBottom: "40px" }}>
              <h2 style={{ marginBottom: "40px" }}>Contact Details</h2>

              <Col span={24}>
                <Row gutter={16}>
                  <Col xs={24} md={5}>
                    <Form.Item
                      name="contact_first_name"
                      rules={[
                        {
                          required: true,
                          max: 10,
                          message: "'Firstname' is invalid",
                        },
                      ]}
                    >
                      <Input placeholder="Firstname" />
                    </Form.Item>
                  </Col>

                  <Col xs={24} md={5}>
                    <Form.Item
                      name="contact_last_name"
                      rules={[
                        {
                          required: true,
                          max: 10,
                          message: "'Lastname' is invalid",
                        },
                      ]}
                    >
                      <Input placeholder="Lastname" />
                    </Form.Item>
                  </Col>
                  <Col xs={24} md={5}>
                    <Form.Item
                      name="contact_mobile_num"
                      rules={[
                        {
                          required: true,
                          len: 10,
                          pattern: "^[0-9]+$",
                          message: "'Mobile' is invalid",
                        },
                      ]}
                    >
                      <Input addonBefore="+91" placeholder="Mobile" />
                    </Form.Item>
                  </Col>

                  <Col xs={24} md={5}>
                    <Form.Item
                      name="contact_email"
                      rules={[
                        {
                          required: true,
                          type: "email",
                          message: "'Email' is invalid",
                        },
                      ]}
                    >
                      <Input placeholder="Email" />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
            </Card>
          )}
          {formType.length > 0 && (
            <Row justify="center">
              <Col xs={24} md={8}>
                <Button
                  block
                  size="large"
                  type="primary"
                  icon={<SendOutlined />}
                  htmlType="submit"
                  //onClick={saveBooking}
                >
                  {formType === "quote" ? "Request a Quote" : "Request a Call"}
                </Button>
              </Col>
            </Row>
          )}
        </div>
      </Form>
    </Layout>
  );
}

export async function getServerSideProps(context) {
  //console.log(context.query);
  return { props: context.query };
}
