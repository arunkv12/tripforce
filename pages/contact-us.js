import Layout from "../layouts";
import { Col, Space, Row } from "antd";
import { MailFilled, MobileFilled } from "@ant-design/icons";

export default function ContactUs() {
  return (
    <Layout>
      <div className="app-padding app-bg-common">
        <h1 className="h1-heading">Contact Us.</h1>

        <Row>
          <Col xs={22} md={6} className="mb-20">
            <a href="mailto: sales@tripforce.in">
              <Space>
                <MailFilled className="font-20" />
                <h2 className="margin-0px">sales@tripforce.in</h2>
              </Space>
            </a>
          </Col>

          <Col xs={22} md={6} className="mb-20">
            <a href="tel: +91 9744956400">
              <Space>
                <MobileFilled className="font-20" />
                <h2 className="margin-0px">+91 9744956400</h2>
              </Space>
            </a>
          </Col>
        </Row>
      </div>
    </Layout>
  );
}
