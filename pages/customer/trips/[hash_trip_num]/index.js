import Layout from "../../../../layouts";
import { useState, useEffect } from "react";
import {
  Collapse,
  Col,
  Row,
  Skeleton,
  Descriptions,
  Result,
  Button,
} from "antd";
import Quotations from "../../../../components/customer/trips/Quotations";
import { async_get_with_pathparam as getwithpath } from "../../../../api/gateway";
import url from "../../../../api/url";
import { formatToDate, formatToDateTime } from "../../../../util/date";

export default function CustomerTrips(props) {
  const { Panel } = Collapse;
  const [dataSource, setDataSource] = useState([]);
  const [loading, setLoading] = useState(true);
  const [showError, setShowError] = useState(false);

  useEffect(() => {
    getwithpath(
      { path1: props.hash_trip_num },
      {
        apiurl: url.customer_trips,
      }
    ).then((res) => {
      setLoading(false);
      if (res.status === 200) {
        setDataSource(res.data);
      } else {
        setShowError(true);
      }
    });
  }, []);

  //useeffect
  useEffect(() => {
    const timer = setTimeout(() => {
      setLoading(false);
    }, 2000);
    return () => clearTimeout(timer);
  }, []);
  //useeffect

  const MoreDetails = () => {
    switch (dataSource.trip_type) {
      case "rfq":
        return (
          <>
            <Descriptions
              className="highlight-description"
              column={{ xs: 1, md: 3 }}
            >
              <Descriptions.Item label="Trip Start Date">
                {formatToDate(dataSource.trip_start_date)}
              </Descriptions.Item>
              <Descriptions.Item label="Trip End Date">
                {formatToDate(dataSource.trip_end_date)}
              </Descriptions.Item>
              <Descriptions.Item label="Trip Start Point">
                {dataSource.trip_start_pt_desc}
              </Descriptions.Item>
              <Descriptions.Item label="Trip End Point">
                {dataSource.trip_end_pt_desc}
              </Descriptions.Item>
              <Descriptions.Item label="Trip Transfer Points">
                {dataSource.trip_transfer_points_desc}
              </Descriptions.Item>
              <Descriptions.Item label="Vehicle Type">
                {dataSource.vehicle_type_desc}
              </Descriptions.Item>
              <Descriptions.Item label="Vehicle Count">
                {dataSource.vehicle_count}
              </Descriptions.Item>
              <Descriptions.Item label="Remark">
                {dataSource.remarks}
              </Descriptions.Item>
            </Descriptions>

            <ContactDetails />
          </>
        );

      case "qb":
        return (
          <>
            <Descriptions
              className="highlight-description"
              column={{ xs: 1, md: 3 }}
            >
              <Descriptions.Item label="Trip Date">
                {formatToDate(dataSource.trip_start_date)}
              </Descriptions.Item>
              <Descriptions.Item label="Trip Point">
                {dataSource.trip_end_pt_desc}
              </Descriptions.Item>
            </Descriptions>

            <ContactDetails />
          </>
        );

      default:
        return <ContactDetails />;
    }
  };

  const ContactDetails = () => (
    <Descriptions
      className="highlight-description"
      column={{ xs: 1, md: 3 }}
      title="Contact Details"
    >
      <Descriptions.Item label="First Name">
        {dataSource.contact_first_name}
      </Descriptions.Item>
      <Descriptions.Item label="Last Name">
        {dataSource.contact_last_name}
      </Descriptions.Item>
      <Descriptions.Item label="Mobile No">
        {dataSource.contact_mobile_country_cd + dataSource.contact_mobile_num}
      </Descriptions.Item>
      <Descriptions.Item label="Email">
        {dataSource.contact_email}
      </Descriptions.Item>
    </Descriptions>
  );

  return (
    <Layout>
      <div className="app-padding app-bg-common">
        {showError ? (
          <Result
            status="warning"
            title="There are some problems with your operation."
            extra={
              <a href="/">
                <Button type="primary" size="large">
                  Vist TripForce
                </Button>
              </a>
            }
          />
        ) : (
          <Skeleton loading={loading} active={loading}>
            <Row justify="center">
              <Col md={22} xs={22}>
                <Descriptions
                  title="Trip Details"
                  className="highlight-description"
                  column={{ xs: 1, md: 3 }}
                >
                  <Descriptions.Item label="Trip Reference No">
                    {dataSource.trip_num}
                  </Descriptions.Item>

                  <Descriptions.Item label="Requestd On">
                    {formatToDateTime(dataSource.created_on, "default")}
                  </Descriptions.Item>
                </Descriptions>

                <MoreDetails />

                <Collapse bordered={false}>
                  <Panel header="Quotations" key="1">
                    <Quotations data={dataSource} />
                  </Panel>
                </Collapse>
              </Col>
            </Row>
          </Skeleton>
        )}
      </div>
    </Layout>
  );
}

export async function getServerSideProps(context) {
  return { props: { hash_trip_num: context.params.hash_trip_num } };
}
