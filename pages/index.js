import {
  Carousel,
  Row,
  Select,
  Col,
  DatePicker,
  Button,
  Form,
  AutoComplete,
} from "antd";
import { useEffect, useState } from "react";
import url from "../api/url";
import BookWithUs from "../components/home/bookwithus";
import Package from "../components/home/package";
import QuickBooking from "../components/home/quickbooking";
import Layout from "../layouts";
import moment from "moment";
import { useRouter } from "next/router";
import { async_get as get } from "../api/gateway";
import tawkTo from "tawkto-react";

export default function Home() {
  //const { Option } = AutoComplete;
  const { Option } = Select;
  const { RangePicker } = DatePicker;
  const [form] = Form.useForm();
  const router = useRouter();
  const [tripPoints, setTripPoints] = useState([]);

  /* useEffect(() => {
    get({ apiurl: url.masters, group_code: "tp" }).then((response) => {
      if (response.status === 200) {
        setTripPoints(response.data);
      }
    });
  }, []); */

  useEffect(() => {
    //tawkTo("60c3152765b7290ac635648f", "1f7t0l9dd"); //@mine
    tawkTo("60fbbed7d6e7610a49acc41e", "1fbbm2uv6"); //@tripforce
  }, []);

  const onSearchTripPoints = (value) => {
    if (value != undefined && value.length > 0) {
      get({ apiurl: url.masters_trip_points, input: value }).then(
        (response) => {
          if (response.status === 200) {
            setTripPoints(response.data);
          }
        }
      );
    }
  };

  const findPreference = (values) => {
    //console.log(values);
    //console.log(tripPoints);
    let data = {};
    data.req_mode = "global";
    data.trip_start_dt = moment().unix();
    data.trip_end_dt = moment().valueOf();
    if (values.trip_date.length > 0) {
      data.trip_start_dt = values.trip_date[0].unix();
      data.trip_end_dt = values.trip_date[1].unix();
    }

    data.trip_end_pt_desc = values.trip_end_pt;
    data.trip_end_pt = tripPoints.filter(
      (item) => item.description === values.trip_end_pt
    )[0].place_id;

    let params = encodeURI(
      Object.keys(data)
        .map((key) => key + "=" + data[key])
        .join("&")
    );

    router.push("/booknow?" + params);
  };

  return (
    <Layout>
      <div>
        <Col xs={0} md={24}>
          <Carousel autoplay dotPosition="left">
            <img src="/bg-3.jpg"></img>
            <img src="/bg-4.jpg"></img>
            {/* <img src="/bg-1.jpg"></img> */}
          </Carousel>
        </Col>

        <div className="global-search global-search-div">
          <Row justify="center">
            <Col xs={22} md={18}>
              <h1>
                Travel safely with <strong>tripforce</strong>
              </h1>
            </Col>
            <Col xs={22} md={18}>
              <div
                style={{
                  boxShadow: "0px 0px 0px 10px rgb(255 255 255 / 30%)",
                }}
              >
                <Form
                  form={form}
                  onFinish={findPreference}
                  layout="horizontal"
                  //className="global-search"
                >
                  {/* <Input.Group
                    compact
                    style={{ width: "100%" }}
                    className="global-search"
                  > */}
                  <Row justify="center">
                    <Col xs={22} md={11}>
                      <Form.Item name="trip_end_pt">
                        <AutoComplete
                          allowClear
                          size="large"
                          filterOption={(input, option) =>
                            option.props.children
                              .toLowerCase()
                              .indexOf(input.toLowerCase()) >= 0
                          }
                          placeholder="Where are you going?"
                          onSearch={onSearchTripPoints}
                        >
                          {tripPoints.map((item) => (
                            <Option
                              key={item["place_id"]}
                              value={item["description"]}
                            >
                              {item["description"]}
                            </Option>
                          ))}
                        </AutoComplete>
                        {/* <Select
                          allowClear
                          showSearch
                          size="large"
                          //style={{ width: "53%" }}
                          filterOption={(input, option) =>
                            option.props.children
                              .toLowerCase()
                              .indexOf(input.toLowerCase()) >= 0
                          }
                          placeholder="Where are you going?"
                          onSearch={onSearchTripPoints}
                        >
                          {tripPoints.map((item) => (
                            <Option value={item["place_id"]}>
                              {item["description"]}
                            </Option>
                          ))}
                        </Select> */}
                      </Form.Item>
                    </Col>

                    <Col xs={22} md={7}>
                      <Form.Item name="trip_date">
                        <RangePicker
                          disabledDate={(currentDate) =>
                            currentDate < moment().subtract(1, "days")
                          }
                          //defaultValue={[moment(), moment().add(1, "days")]}
                          style={{ padding: "14px", width: "100%" }}
                          size="large"
                        />
                      </Form.Item>
                    </Col>

                    <Col xs={22} md={6}>
                      <Form.Item shouldUpdate>
                        {() => (
                          <Button
                            disabled={!form.isFieldsTouched(true)}
                            block
                            size="large"
                            style={{ height: "55px" }}
                            type="primary"
                            htmlType="submit"
                          >
                            Let's find your preference
                          </Button>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Form>
              </div>
            </Col>
          </Row>
        </div>
      </div>

      <Package />
      <BookWithUs />
      <QuickBooking />
    </Layout>
  );
}
