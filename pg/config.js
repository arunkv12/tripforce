//const connectionString = "postgresql://postgres:postgres@localhost:5432/tripforce?schema=public";

const connectionString = process.env.DATABASE_URL;
const Pool = require("pg").Pool;
const pool = new Pool({ connectionString });

module.exports = pool;
