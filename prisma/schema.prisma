generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

model tf_customers {
  id                Int        @id @default(autoincrement())
  customer_email    String     @unique @db.VarChar(255)
  first_name        String     @db.VarChar(100)
  last_name         String     @db.VarChar(100)
  mobile_country_cd String?    @db.VarChar(5)
  verified          Int
  status            Int
  created_by        String?    @db.VarChar(100)
  created_on        DateTime?  @db.Timestamp(6)
  updated_by        String?    @db.VarChar(100)
  updated_on        DateTime?  @db.Timestamp(6)
  mobile_num        String?    @db.VarChar(10)
  tf_trips          tf_trips[]
}

model tf_mst_data {
  id                Int       @id @default(autoincrement())
  group_code        String    @db.VarChar(10)
  reference_code    String    @unique @db.VarChar(10)
  reference_desc_en String    @db.VarChar(100)
  status            Int
  created_by        String?   @db.VarChar(100)
  created_on        DateTime? @db.Timestamp(6)
  updated_by        String?   @db.VarChar(100)
  updated_on        DateTime? @db.Timestamp(6)
}

model tf_quotations {
  id                   Int                   @id
  to_first_name        String?               @db.VarChar(100)
  to_last_name         String?               @db.VarChar(100)
  to_email             String?               @db.VarChar(100)
  to_mobile_country_cd String?               @db.VarChar(5)
  to_mobile_num        String?               @db.VarChar(10)
  discount             Int?                  @db.SmallInt
  tax                  Int?                  @db.SmallInt
  status               Int                   @db.SmallInt
  created_by           String?               @db.VarChar(100)
  created_on           DateTime?             @db.Timestamp(6)
  updated_by           String?               @db.VarChar(100)
  updated_on           DateTime?             @db.Timestamp(6)
  quotation_num        String                @unique @db.VarChar
  trip_num             String                @db.VarChar(100)
  tf_trips             tf_trips              @relation(fields: [trip_num], references: [trip_num])
  tf_quotations_items  tf_quotations_items[]

  @@index([trip_num], name: "fki_trip_num_fkey")
}

model tf_quotations_items {
  id            Int           @id @default(autoincrement())
  item          String        @db.VarChar(100)
  quantity      Int           @db.SmallInt
  rate          Decimal       @db.Decimal
  status        Int           @db.SmallInt
  created_by    String?       @db.VarChar(100)
  created_on    DateTime?     @db.Timestamp(6)
  updated_by    String?       @db.VarChar(100)
  updated_on    DateTime?     @db.Timestamp(6)
  quotation_num String        @db.VarChar
  tf_quotations tf_quotations @relation(fields: [quotation_num], references: [quotation_num])

  @@index([quotation_num], name: "fki_tf_quotations_num_fkey")
}

model tf_records {
  id          Int       @id @default(autoincrement())
  type        String    @db.VarChar(15)
  record      String    @db.VarChar(800)
  read_status Int       @db.SmallInt
  target_to   String    @db.VarChar(10)
  status      Int       @db.SmallInt
  created_by  String?   @db.VarChar(100)
  created_on  DateTime? @db.Timestamp(6)
  updated_by  String?   @db.VarChar(100)
  updated_on  DateTime? @db.Timestamp(6)
  trip_num    String    @db.VarChar(100)
  tf_trips    tf_trips  @relation(fields: [trip_num], references: [trip_num])

  @@index([trip_num], name: "fki_tf_records_trip_num_fkey")
}

model tf_trips {
  id                        Int             @id
  customer_email            String          @db.VarChar(255)
  trip_type                 String          @db.VarChar(10)
  edit_lock                 Int
  trip_start_date           DateTime?       @db.Timestamp(6)
  trip_start_pt             String?         @db.VarChar(400)
  trip_end_pt               String?         @db.VarChar(400)
  trip_transfer_points      String?         @db.VarChar(800)
  head_count                Int?            @db.SmallInt
  vehicle_type              String?         @db.VarChar(10)
  vehicle_count             Int?            @db.SmallInt
  remarks                   String?         @db.VarChar(400)
  contact_email             String?         @db.VarChar(100)
  contact_mobile_country_cd String?         @db.VarChar(5)
  contact_first_name        String?         @db.VarChar(100)
  contact_last_name         String?         @db.VarChar(100)
  status                    Int             @db.SmallInt
  created_by                String?         @db.VarChar(100)
  created_on                DateTime?       @db.Timestamp(6)
  updated_by                String?         @db.VarChar(100)
  updated_on                DateTime?       @db.Timestamp(6)
  trip_end_date             DateTime?       @db.Timestamp(6)
  contact_mobile_num        String?         @db.VarChar(10)
  trip_num                  String          @unique @db.VarChar(100)
  trip_start_pt_desc        String?         @db.VarChar
  trip_end_pt_desc          String?         @db.VarChar
  trip_transfer_points_desc String?         @db.VarChar
  tf_customers              tf_customers    @relation(fields: [customer_email], references: [customer_email])
  tf_quotations             tf_quotations[]
  tf_records                tf_records[]
}

model tf_users {
  id             Int       @id @default(autoincrement())
  user_email     String    @unique @db.VarChar(255)
  first_name     String    @db.VarChar(100)
  last_name      String    @db.VarChar(100)
  user_pwd       String    @db.VarChar(100)
  roles          String    @db.VarChar(400)
  profile_status Int
  login_status   Int
  created_by     String?   @db.VarChar(100)
  created_on     DateTime? @db.Timestamp(6)
  updated_by     String?   @db.VarChar(100)
  updated_on     DateTime? @db.Timestamp(6)
}
