var express = require("express");
var router = express.Router();

const { authenticate, authorize } = require("../controllers/security");

const masters = require("../controllers/masters");
const bookings = require("../controllers/bookings");
const adminTripsRouter = require("../controllers/admin/trips");
const adminQuotationsRouter = require("../controllers/admin/trips/quotations");
const customerTripsRouter = require("../controllers/customer/trips");
const customerQuotationsRouter = require("../controllers/customer/trips/quotations");

const authRouter = express.Router();
authRouter.post("/users/auth", authenticate);

router.use("/masters", masters);
router.use("/bookings", bookings);

//admin endpoints
router.use("/admin/trips", authorize, adminTripsRouter);
router.use("/admin/trips/quotations", authorize, adminQuotationsRouter);

//customer
router.use("/customer/trips", customerTripsRouter);
router.use("/customer/trips/quotations", customerQuotationsRouter);

module.exports = { router, authRouter };
