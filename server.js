const express = require("express");
const next = require("next");
const { router, authRouter } = require("./routes");

const port = parseInt(process.env.PORT, 10) || 80;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();
const dotenv = require("dotenv");
dotenv.config();

app.prepare().then(() => {
  const server = express();
  server.use(express.json({ limit: "3mb" }));

  server.use("/api", router, authRouter);

  server.all("*", (req, res) => {
    return handle(req, res);
  });

  server.listen(port, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });
});
