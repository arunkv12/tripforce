import moment from "moment";

export function getDateTimeString(momentObj) {
  return momentObj.format("YYYY-MM-DD HH:mm:ss");
}

export function getDateString(momentObj) {
  return momentObj.format("YYYY-MM-DD");
}

export function formatToDateTime(dateStringUTC, type) {
  if (type === "default") {
    return moment(moment.utc(dateStringUTC).toDate()).format(
      "DD-MMM-YYYY HH:mm:ss"
    );
  } else {
    return moment(dateStringUTC)
      .subtract(moment().utcOffset() / 60, "h")
      .format("DD-MMM-YYYY HH:mm:ss");
  }
}

export function formatToDate(dateStringUTC) {
  return dateStringUTC != undefined && dateStringUTC.length > 0
    ? moment(moment.utc(dateStringUTC).toDate()).format("DD-MMM-YYYY")
    : "";
}
