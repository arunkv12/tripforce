const messages = {
  required: "'${label}' is required!",
  types: {
    email: "'${label}' is not valid!",
  },
  string: {
    len: "'${label}' must be exactly ${len} characters",
    min: "'${label}' must be at least ${min} characters",
    max: "'${label}' cannot be longer than ${max} characters",
    range: "'${label}' must be between ${min} and ${max} characters",
  },
  pattern: {
    mismatch: "'${label}' has invalid characters",
  },
};

export { messages };
